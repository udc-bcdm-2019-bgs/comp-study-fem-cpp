# Comparative study of C++ libraries for the finite element method

This is the repository of the ***Comparative study of C++ libraries for the finite element method***, a project developed during the departmental collaboration grant in research work awarded to Borja González Seoane, in the Department of Mathematics of the University of Coruña, year 2019-20.

The project consists in a comparative study of C++ libraries of the finite element method, using to test the performance the Poisson equation.

This repository only contains the code used in the study, the study itself is available at *pending to define URL*.

The only one dependency of this software is Docker and, only if you want to use the analysis module, Python \~3.8.


## List of C++ libraries considered

1. FEniCS.
2. Deal II.
3. Oomph-lib.


## Use

Each library folder includes detailed information about how to run but all of them follows an unified approach. You usually only need to pull the proper Docker image for each library and then run the proper script `./run_into_docker.sh` with the desired processes and dimension values.

The `analysis` folder includes scripts to compose graphics with the obtained results.


## Docker images available

All the used images are public and uploaded to the Docker Hub into repositories of the `udcbcdm2019bgs` Docker Hub organization profile.

| Library | Command | Link |
|-|-|-|
| FEniCS | `docker pull udcbcdm2019bgs/fenics` | [View on Docker Hub](https://hub.docker.com/r/udcbcdm2019bgs/fenics) |
| Deal II | `docker pull udcbcdm2019bgs/dealii` | [View on Docker Hub](https://hub.docker.com/r/udcbcdm2019bgs/dealii) |
| Oomph-lib | `docker pull udcbcdm2019bgs/oomph-lib` | [View on Docker Hub](https://hub.docker.com/r/udcbcdm2019bgs/oomph-lib) |

Simply use `docker pull <image-name>` to pull them.
