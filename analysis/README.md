# Analysis module

This module closure a set of scripts to summarize the information recompiled during the performance tests of the other modules, elaborating graphics and tables to use in technical reports or other documents.

This module in wrote in Python and uses a Python virtual environment to collect all its dependencies.


## Use

The recommended way to work with this module is:

1. Create a Python virtual environment. There are several tools to achieve this, for example Pyenv.

```sh
# Create and activate a a Python virtual environment with Pyenv
pyenv virtualenv 3.8.0 comp-study-fem-cpp-analysis
pyenv activate comp-study-fem-cpp-analysis
```

2. Install all the dependencies.

```sh
pip install -r requirements.txt
```

3. Now you can use the Python `comp_study_fem_cpp_analysis` package simply running, from the `analysis` directory:

```sh
python -m comp_study_fem_cpp_analysis
```

You probably are interesting in take a look to the `comp_study_fem_cpp_analysis/__main__.py` to select what plot. Per omission, the program plots all the available graphics and save them as images in this directory.
