# -*- coding: utf-8 -*-

###########################################################
# Analysis module
#
# Comparative study of C++ libraries for the finite
# element method
#
# Copyright 2020 Borja González Seoane
#
# Contact: borja.gseoane@udc.es
###########################################################

__author__ = 'Borja González Seoane'
__copyright__ = 'Copyright 2020, Borja González Seoane'
__credits__ = 'Borja González Seoane'
__license__ = '../LICENSE'
__maintainer__ = 'Borja González Seoane'
__email__ = 'borja.gseoane@udc.es'
