# -*- coding: utf-8 -*-

###########################################################
# Analysis module
#
# Comparative study of C++ libraries for the finite
# element method
#
# Copyright 2020 Borja González Seoane
#
# Contact: borja.gseoane@udc.es
###########################################################

"""'main'

This file allows to simply configure your desired plots. Per omission simply
run all the possible and save them in the parent directories as images.
"""

import matplotlib
import matplotlib.pyplot as plt

from comp_study_fem_cpp_analysis.catch_data import get_machine_data
from comp_study_fem_cpp_analysis.plots import plot_times_mesh, \
    plot_memory_mesh, plot_memory_comparation, plot_times_comparation

if __name__ == '__main__':
    #######################################################
    """ Editing these variables you can generate all the graphs for all the 
    libraries """

    # Between 'fenics', 'dealii' and 'oomph-lib'. You can add several
    libs = ['fenics', 'dealii', 'oomph-lib']

    """You can simply put here a result obtained with the machine that you 
    want to plot to select that machine. Next is our UDC's reference machine"""
    machine = '../fenics/results/20060918261591719966_p1_m32'
    machine_register = get_machine_data(machine)

    # Limits for the plots
    desired_max_processes = 8
    desired_max_size = 1024
    #######################################################

    # Plots
    for lib in libs:
        plot_times_mesh(lib,
                        machine_register,
                        desired_processes=desired_max_processes,
                        desired_max_size=desired_max_size,
                        plot_function=matplotlib.pyplot.plot,
                        save_flag=True,
                        output_filepath=f'{lib}-times-plot.png')
        plot_memory_mesh(lib,
                         machine_register,
                         desired_processes=desired_max_processes,
                         desired_max_size=desired_max_size,
                         plot_function=matplotlib.pyplot.plot,
                         save_flag=True,
                         output_filepath=f'{lib}-memory-plot.png')

        plot_times_mesh(lib,
                        machine_register,
                        desired_processes=desired_max_processes,
                        desired_max_size=desired_max_size,
                        plot_function=matplotlib.pyplot.loglog,
                        save_flag=True,
                        output_filepath=f'{lib}-times-loglog.png')
        plot_memory_mesh(lib,
                         machine_register,
                         desired_processes=desired_max_processes,
                         desired_max_size=desired_max_size,
                         plot_function=matplotlib.pyplot.loglog,
                         save_flag=True,
                         output_filepath=f'{lib}-memory-loglog.png')

        plot_times_mesh(lib,
                        machine_register,
                        desired_processes=desired_max_processes,
                        desired_max_size=desired_max_size,
                        plot_function=matplotlib.pyplot.semilogx,
                        save_flag=True,
                        output_filepath=f'{lib}-times-semilogx.png')
        plot_memory_mesh(lib,
                         machine_register,
                         desired_processes=desired_max_processes,
                         desired_max_size=desired_max_size,
                         plot_function=matplotlib.pyplot.semilogx,
                         save_flag=True,
                         output_filepath=f'{lib}-memory-semilogx.png')
    plot_times_comparation(libs,
                           machine_register,
                           desired_processes=desired_max_processes,
                           desired_max_size=desired_max_size,
                           plot_function=matplotlib.pyplot.semilogx,
                           save_flag=True,
                           output_filepath=f'all-times-semilogx.png')
    plot_memory_comparation(libs,
                            machine_register,
                            desired_processes=desired_max_processes,
                            desired_max_size=desired_max_size,
                            plot_function=matplotlib.pyplot.semilogx,
                            save_flag=True,
                            output_filepath=f'all-memory-semilogx.png')
