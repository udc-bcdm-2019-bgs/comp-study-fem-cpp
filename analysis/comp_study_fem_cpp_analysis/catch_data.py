# -*- coding: utf-8 -*-

###########################################################
# Analysis module
#
# Comparative study of C++ libraries for the finite
# element method
#
# Copyright 2020 Borja González Seoane
#
# Contact: borja.gseoane@udc.es
###########################################################

"""'catch_data'

This file closures some functions to catch data from the obtained results
directories of the studied libraries.
"""

import os
import re
from glob import glob

from deprecated import deprecated


class NoisySampleError(Exception):
    """This class is used to treat wrong terminated samples which could
    pollute the analysis."""
    pass


class UnavailableReport(Exception):
    """This exception is used to raise when a sample folder does not
    contain a type of report, usually due to version differences between the
    time of the essay and the current moment."""
    pass


def get_processes(dirpath: str) -> int:
    """Return the number of processes used in a performance test with the
    software of this project, given the directory full relative path since the
    home directory of the project.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    processes_chunk = dirpath.split('/')[3].split('_')[1]
    return int(re.findall(r'\d+', processes_chunk)[0])


def get_mesh_size(dirpath: str) -> int:
    """Return the mesh size used in a performance test with the software of
    this project, given the directory full relative path since the home
    directory of the project.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    mesh_chunk = dirpath.split('/')[3].split('_')[2]
    return int(re.findall(r'\d+', mesh_chunk)[0])


def get_machine_data(dirpath: str) -> str:
    """Return the machine data used in a performance test with the software of
    this project, given the directory full relative path since the home
    directory of the project.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    with open(dirpath + '/lscpu-report.txt', 'rb') as file:
        lines = file.readlines()
        """ Discard CPU MHz data of the machine register due to it is not 
        constant between several registers of the same machine.
        
        "Modern cpu's can operate at several different frequencies changing
        dynamically under the load requirements. Intel call this SpeedStep. 
        When a cpu has little to do it will run at a lower frequency to 
        reduce power (and therefore heat and fan noise)"
        [Stack Overflow, https://unix.stackexchange.com/a/295356].
        """
        for line in lines:
            if 'CPU MHz' in str(line):
                lines.remove(line)
            if 'BogoMIPS' in str(line):
                lines.remove(line)
    return ''.join([str(l) for l in lines])


def get_machine_data_resume(machine_register: str) -> str:
    """Return the machine data used in a performance test with the software of
    this project, but in a reduced way than 'get_machine_data'.

    @param machine_register: the replay of the function 'get_machine_data' for
        a given machine
    """
    lines = machine_register.split('\\n')
    interesting_lines = []
    for l in lines:
        if 'Model name' in l:
            interesting_lines.insert(0, ' '.join(l.split(': ')[1].split()))
            # Remove headers and extra spaces
        if 'CPU(s):    ' in l:
            interesting_lines.insert(1, ' with '
                                     + ' '.join(l.split(': ')[1].split())
                                     + ' cores')
        if 'Thread(s) per core' in l:
            interesting_lines.insert(2, '.\nThreads per core: '
                                     + ' '.join(l.split(': ')[1].split()))
        if 'CPU min MHz' in l:
            interesting_lines.insert(3, '. CPU ratio (MHz): '
                                     + ' '.join(l.split(': ')[1].split()))
        if 'CPU max MHz' in l:
            interesting_lines.insert(4, ' to '
                                     + ' '.join(l.split(': ')[1].split()))

    interesting_lines = ''.join(interesting_lines)
    return interesting_lines


def get_wall_clock_time(dirpath: str) -> int:
    """Return the measured wall clock time in a performance test with the
    software of this project, given the directory full relative path since
    the home directory of the project.

    This function is sensible to noisy samples.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    try:
        file = open(dirpath + '/times-report.txt', 'rb')
    except FileNotFoundError:
        try:
            file = open(dirpath + '/measures.txt', 'rb')  # Old
        except FileNotFoundError:
            raise FileNotFoundError(dirpath)  # Real error with input path
    lines = file.readlines()
    file.close()
    time_chunks = []
    valid_sample_flag = False
    valid_sample_flag_lock = False
    for line in lines:
        line = str(line)
        if 'Exit status: 0' in line and not valid_sample_flag_lock:  #
            # Check if noisy sample
            valid_sample_flag = True
        if 'std::bad_alloc' in line:
            """A detected error in the logs which do not affect to final 
            'Exit status'."""
            valid_sample_flag = False
            valid_sample_flag_lock = True
        if 'Elapsed (wall clock) time' in line:
            time_chunks = re.findall(r'\d+', line)
    if not valid_sample_flag:
        raise NoisySampleError
    return int(time_chunks[0]) * 360 + int(time_chunks[1]) * 60 + int(
        time_chunks[2])


@deprecated
def get_used_memory_gnu_time(dirpath: str) -> int:
    """Return the measured used maximum consecutive memory in a
    performance test with the software of this project, given the directory
    full relative path since the home directory of the project. Catch the
    data of GNU Time tool.

    This function do not support well parallelism. Returns the maximum
    allocated memory chunk, but in a parallel execution does not compute all
    the child.

    This function is sensible to noisy samples.

    The return value is in MiB.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    try:
        file = open(dirpath + '/times-report.txt', 'rb')
    except FileNotFoundError:
        try:
            file = open(dirpath + '/measures.txt', 'rb')  # Old
        except FileNotFoundError:
            raise FileNotFoundError(dirpath)  # Real error with input path
    lines = file.readlines()
    file.close()
    time_chunks = []
    valid_sample_flag = False
    valid_sample_flag_lock = False
    for line in lines:
        line = str(line)
        if 'Exit status: 0' in line and not valid_sample_flag_lock:  #
            # Check if noisy sample
            valid_sample_flag = True
        if 'std::bad_alloc' in line:
            """A detected error in the logs which do not affect to final 
            'Exit status'."""
            valid_sample_flag = False
            valid_sample_flag_lock = True
        if 'Maximum resident set size' in line:
            time_chunks = re.findall(r'\d+', line)
    if not valid_sample_flag:
        raise NoisySampleError
    return int(time_chunks[0]) // 1024


def get_used_memory_mprof(dirpath: str) -> int:
    """Return the measured used memory time in a performance test with the
    software of this project, given the directory full relative path since
    the home directory of the project. Catch the data of M-Prof tool.

    This function is not sensible to noisy samples, so it is recommended to
    use this function combined with one of the other ones.

    The return value is in MiB.

    @param dirpath: the directory full relative path since the
        home directory of the project of the source. Format example:
        'fenics/results/20022413271582550860_p4_m512'
    """
    try:
        file = open(glob(dirpath + '/*.dat')[0], 'rb')
    except IndexError:  # Empty list, so not '*.dat'-like file in path
        raise UnavailableReport(dirpath)  # Real error with input path
    lines = file.readlines()
    file.close()

    mem_values = []
    for line in lines:
        line = str(line)
        if 'MEM' in line:  # We need the last apparition of 'MEM'
            # The first one is the time, the second (so the index 1) the memory
            mem_values.append(float(re.findall(r'\d+.\d+', line)[0]))

    if mem_values:
        return int(max(mem_values))  # The max allocated memory
    else:
        raise UnavailableReport


def get_all_samples_of_library(library_name: str) -> [str]:
    """Return a list with the generated paths of all the samples of a
    studied library between: 'fenics', 'dealii', 'moose', 'oomph-lib', etc.

    @param library_name: simply the name of the library to explore between
        the ofered in the studied. The name in just the subdirectory name
        from the project home directory. E.g.: 'fenics' or 'dealii'
    """
    dirpath = '../' + library_name + '/results'
    # Take path since project home directory
    samples = [x[0] for x in os.walk(dirpath)]
    return samples[1:]  # Remove the first instance, which is the self dir
