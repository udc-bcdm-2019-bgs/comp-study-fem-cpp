# -*- coding: utf-8 -*-

###########################################################
# Analysis module
#
# Comparative study of C++ libraries for the finite
# element method
#
# Copyright 2020 Borja González Seoane
#
# Contact: borja.gseoane@udc.es
###########################################################

"""'plots'

This file closures some functions to generate plot graphics to represent the
data of the study.
"""

import math
import statistics
import time

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from comp_study_fem_cpp_analysis.catch_data import *


def get_samples_grouped_by_processes(library_name: str,
                                     machine_register: str = None,
                                     desired_processes: int = None) \
        -> ([str], str):
    """Returns a list of samples grouped by involved processes in their
    execution.

    This is an auxiliary function for plot ones.

    @param library_name: simply the name of the library to explore between
        the ofered in the studied. The name in just the subdirectory name
        from the project home directory. E.g.: 'fenics' or 'dealii'
    @param machine_register: the register of a given machine obtained with
        the function 'catch_data.get_machine_data'. It is used to filter the
        data so only the samples obtained with the input machine will be
        plotted. It it is omitted, it will be plotted the data of the machine
        used in more samples
    @param desired_processes: a filter to limit the quantity of desired
        processes
    """
    all_samples = get_all_samples_of_library(library_name)

    # Group by machine
    machines = []
    for s in all_samples:
        m = get_machine_data(s)
        if m not in machines:
            machines.append(m)
    samples_by_machine = [{'machine': m,
                           'samples': [s for s in all_samples
                                       if get_machine_data(s) == m]
                           } for m in machines]
    interesting_samples = []
    if machine_register:
        for i in samples_by_machine:
            if i['machine'] == machine_register:
                interesting_samples = i['samples']
                break
    else:
        for i in samples_by_machine:
            if len(i['samples']) > len(interesting_samples):
                machine_register = i['machine']
                interesting_samples = i['samples']

    if not interesting_samples:
        raise ValueError(machine_register, "The given machine has not been "
                                           "used yet to do any essay!")

    # Get max of processes used
    max_processes = max([get_processes(s) for s in interesting_samples])

    # Get max of processes desired
    if desired_processes:
        desired_processes = min(max_processes, desired_processes)
    else:
        desired_processes = max_processes

    # Group by processes
    samples_by_processes = []
    for i in range(0, desired_processes):
        samples_by_processes.append(
            [s for s in interesting_samples if get_processes(s) == i + 1])

    return samples_by_processes, machine_register


def plot_times_mesh(library_name: str,
                    machine_register: str = None,
                    desired_processes: int = None,
                    desired_max_size: int = math.inf,
                    plot_function: callable = matplotlib.pyplot.plot,
                    save_flag: bool = False,
                    output_filepath: str = None) -> None:
    """Plot a graphic comparing computation times and mesh size scalability.

    @param library_name: simply the name of the library to explore between
        the ofered in the studied. The name in just the subdirectory name
        from the project home directory. E.g.: 'fenics' or 'dealii'
    @param machine_register: the register of a given machine obtained with
        the function 'catch_data.get_machine_data'. It is used to filter the
        data so only the samples obtained with the input machine will be
        plotted. It it is omitted, it will be plotted the data of the machine
        used in more samples
    @param desired_processes: a filter to limit the quantity of desired
        processes
    @param desired_max_size: a filter to limit the mesh size for plotted results
    @param plot_function: the plot style between 'matplotlib.pyplot.plot' (
        per omission) and 'matplotlib.pyplot.loglog', for a logarithmical
        display
    @param save_flag: set to false (per omission) plots the graphic, set to
        true save the plot to a file.
    @param output_filepath: the relative path and name to the output plot
        file, if using 'save_flag=True'. If it is omitted a file named based
        in the time stamp will be saved in the current directory.
    """
    samples_by_processes, machine_register = \
        get_samples_grouped_by_processes(library_name,
                                         machine_register,
                                         desired_processes)

    # Refine results: mean times
    refined_samples = []
    for sample_by_processes in samples_by_processes:
        try:
            sizes_and_times = [{'size': get_mesh_size(s),
                                'time': get_wall_clock_time(s)} for s in
                               sample_by_processes]
        except NoisySampleError:
            pass
        sizes = set([s['size'] for s in sizes_and_times])
        refined_samples_label = []
        for size in sizes:
            if size <= desired_max_size:
                mean_time = statistics.mean([s['time'] for s in sizes_and_times
                                             if s['size'] == size])
                refined_samples_label.append({'size': size, 'time': mean_time})
        refined_samples.append(refined_samples_label)

    # Sort by mesh size
    for samples in refined_samples:
        samples.sort(key=lambda s: s['size'])

    # Use a graphic per processes group
    plt.figure()
    for i in range(0, len(refined_samples)):
        x = [s['size'] for s in refined_samples[i]]
        y = [s['time'] for s in refined_samples[i]]

        # Different markers for odds and evens
        if not i % 2:  # Odd
            marker = 'P'
        else:  # Even
            marker = 'x'

        # Plotting the points
        plot_function(x, y, label=str(i + 1) + ' process', marker=marker)

    # Decoration
    plt.xlabel('mesh sizes (in points)')
    plt.ylabel('times (in seconds)')
    plt.suptitle(f'Time / mesh size scalability with {library_name.upper()} '
                 f'library')
    plt.title('\nMachine: ' + get_machine_data_resume(machine_register),
              fontdict={'fontsize': 'small'})
    plt.legend()

    # Show or save to a file the plot
    if save_flag:
        if not output_filepath:
            output_filepath = str(time.time_ns())  # Use timestamp to file name
        plt.savefig(output_filepath)
    else:
        plt.show()


# noinspection PyShadowingNames
def plot_memory_mesh(library_name: str,
                     machine_register: str = None,
                     desired_processes: int = None,
                     desired_max_size: int = math.inf,
                     source: str = 'mprof',
                     plot_function: callable = matplotlib.pyplot.plot,
                     save_flag: bool = False,
                     output_filepath: str = None) -> None:
    """Plot a graphic comparing maximum used memory and mesh size
    scalability.

    @param library_name: simply the name of the library to explore between
        the ofered in the studied. The name in just the subdirectory name
        from the project home directory. E.g.: 'fenics' or 'dealii'
    @param machine_register: the register of a given machine obtained with
        the function 'catch_data.get_machine_data'. It is used to filter the
        data so only the samples obtained with the input machine will be
        plotted. It it is omitted, it will be plotted the data of the machine
        used in more samples
    @param desired_processes: a filter to limit the quantity of desired
        processes
    @param desired_max_size: a filter to limit the mesh size for plotted results
    @param source: the profiling tool source to use. Omission or 'None' implies
        'mprof'. Options are: 'mprof', 'gnu-time' and 'all', which means all.
    @param plot_function: the plot style between 'matplotlib.pyplot.plot' (
        per omission) and 'matplotlib.pyplot.loglog', for a logarithmical
        display
    @param save_flag: set to false (per omission) plots the graphic, set to
        true save the plot to a file.
    @param output_filepath: the relative path and name to the output plot
        file, if using 'save_flag=True'. If it is omitted a file named based
        in the time stamp will be saved in the current directory.
    """
    samples_by_processes, machine_register = \
        get_samples_grouped_by_processes(library_name,
                                         machine_register,
                                         desired_processes)

    # Configure mode
    if source == 'mprof':
        source = get_used_memory_mprof
    elif source == 'gnu-time':
        source = get_used_memory_gnu_time
    elif source == 'all':
        source = None
    else:
        raise ValueError(source)

    # Refine results: mean use of memory
    refined_samples = []
    for i in range(0, len(samples_by_processes)):
        sizes_and_times = []
        try:
            for s in samples_by_processes[i]:
                if source:
                    try:
                        mem_reg = source(s)
                    except UnavailableReport:
                        continue  # Impossible use this source
                else:
                    gnu_time_mem_reg = get_used_memory_gnu_time(s)
                    try:
                        # Merge the two register sources into one value
                        mem_reg = statistics.mean([gnu_time_mem_reg,
                                                   get_used_memory_mprof(s)])
                    except UnavailableReport:
                        mem_reg = gnu_time_mem_reg

                sizes_and_times.append({'size': get_mesh_size(s),
                                        'memory': mem_reg})
        except NoisySampleError:
            pass
        sizes = set([s['size'] for s in sizes_and_times])
        refined_samples_label = []
        for size in sizes:
            if size <= desired_max_size:
                mean_memory = statistics.mean(
                    [s['memory'] for s in sizes_and_times
                     if s['size'] == size])
                refined_samples_label.append(
                    {'size': size, 'memory': mean_memory})
        refined_samples.append(refined_samples_label)

    # Sort by mesh size
    for samples in refined_samples:
        samples.sort(key=lambda s: s['size'])

    # Use a graphic per processes group
    plt.figure()
    for i in range(0, len(refined_samples)):
        x = [s['size'] for s in refined_samples[i]]
        y = [s['memory'] for s in refined_samples[i]]

        # Different markers for odds and evens
        if not i % 2:  # Odd
            marker = 'P'
        else:  # Even
            marker = 'x'

        # Plotting the points
        plot_function(x, y, label=str(i + 1) + ' process', marker=marker)

    # Decoration
    plt.xlabel('mesh sizes (in points)')
    plt.ylabel('memory used (in MiB)')
    plt.suptitle(f'Used memory / mesh size scalability with '
                 f'{library_name.upper()} library')
    plt.title('\nMachine: ' + get_machine_data_resume(machine_register),
              fontdict={'fontsize': 'small'})
    plt.legend()

    # Show or save to a file the plot
    if save_flag:
        if not output_filepath:
            output_filepath = str(time.time_ns())  # Use timestamp to file name
        plt.savefig(output_filepath)
    else:
        plt.show()


# noinspection PyShadowingNames
def plot_times_comparation(library_names: [str],
                           machine_register: str = None,
                           desired_processes: int = None,
                           desired_max_size: int = math.inf,
                           plot_function: callable = matplotlib.pyplot.plot,
                           save_flag: bool = False,
                           output_filepath: str = None) -> None:
    """Plot a graphic comparing computation times of various libraries

    @param library_names: simply a list with the names of the libraries to
        explore between the offered in the studied. The name in just the
        subdirectory name from the project home directory. E.g.: 'fenics'
        or 'dealii'
    @param machine_register: the register of a given machine obtained with
        the function 'catch_data.get_machine_data'. It is used to filter the
        data so only the samples obtained with the input machine will be
        plotted. It it is omitted, it will be plotted the data of the machine
        used in more samples
    @param desired_processes: a filter to limit the quantity of desired
        processes
    @param desired_max_size: a filter to limit the mesh size for plotted results
    @param plot_function: the plot style between 'matplotlib.pyplot.plot' (
        per omission) and 'matplotlib.pyplot.loglog', for a logarithmical
        display
    @param save_flag: set to false (per omission) plots the graphic, set to
        true save the plot to a file.
    @param output_filepath: the relative path and name to the output plot
        file, if using 'save_flag=True'. If it is omitted a file named based
        in the time stamp will be saved in the current directory.
    """
    plt.figure()  # Necessary to refresh if there is a previous plot charged

    for library_name in library_names:
        samples_by_processes, machine_register = \
            get_samples_grouped_by_processes(library_name,
                                             machine_register,
                                             desired_processes)

        # Refine results: mean times
        refined_samples = []
        for sample_by_processes in samples_by_processes:
            try:
                sizes_and_times = [{'size': get_mesh_size(s),
                                    'time': get_wall_clock_time(s)} for s in
                                   sample_by_processes]
            except NoisySampleError:
                pass
            sizes = set([s['size'] for s in sizes_and_times])
            refined_samples_label = []
            for size in sizes:
                if size <= desired_max_size:
                    mean_time = statistics.mean(
                        [s['time'] for s in sizes_and_times
                         if s['size'] == size])
                    refined_samples_label.append(
                        {'size': size, 'time': mean_time})
            refined_samples.append(refined_samples_label)

        # Sort by mesh size
        for samples in refined_samples:
            samples.sort(key=lambda s: s['size'])

        for i in range(0, len(refined_samples)):
            x = [s['size'] for s in refined_samples[i]]
            y = [s['time'] for s in refined_samples[i]]

            # Use a color for each library
            if library_name == 'fenics':
                color = 'red'
            elif library_name == 'dealii':
                color = 'blue'
            elif library_name == 'oomph-lib':
                color = 'green'
            else:
                color = 'black'

            # # Different markers for odds and evens
            # if not i % 2:  # Odd
            #     marker = 'P'
            # else:  # Even
            #     marker = 'x'

            # Plotting the points
            plot_function(x, y, color=color)  # Add 'marker = marker' to use it

    # Decoration
    plt.xlabel('mesh sizes (in points)')
    plt.ylabel('times (in seconds)')
    plt.suptitle(
        f'Time / mesh size scalability of each library')
    plt.title('\nMachine: ' + get_machine_data_resume(machine_register),
              fontdict={'fontsize': 'small'})
    fenics_legend_label = mpatches.Patch(color='red', label='FEniCS')
    dealii_legend_label = mpatches.Patch(color='blue', label='Deal II')
    oomph_lib_legend_label = mpatches.Patch(color='green', label='Oomph-lib')
    plt.legend(handles=[fenics_legend_label, dealii_legend_label,
                        oomph_lib_legend_label])

    # Show or save to a file the plot
    if save_flag:
        if not output_filepath:
            output_filepath = str(
                time.time_ns())  # Use timestamp to file name
        plt.savefig(output_filepath)
    else:
        plt.show()


# noinspection PyShadowingNames
def plot_memory_comparation(library_names: [str],
                            machine_register: str = None,
                            desired_processes: int = None,
                            desired_max_size: int = math.inf,
                            source: str = 'mprof',
                            plot_function: callable = matplotlib.pyplot.plot,
                            save_flag: bool = False,
                            output_filepath: str = None) -> None:
    """Plot a graphic comparing computation times of various libraries

    @param library_names: simply a list with the names of the libraries to
        explore between the offered in the studied. The name in just the
        subdirectory name from the project home directory. E.g.: 'fenics'
        or 'dealii'
    @param machine_register: the register of a given machine obtained with
        the function 'catch_data.get_machine_data'. It is used to filter the
        data so only the samples obtained with the input machine will be
        plotted. It it is omitted, it will be plotted the data of the machine
        used in more samples
    @param desired_processes: a filter to limit the quantity of desired
        processes
    @param desired_max_size: a filter to limit the mesh size for plotted results
    @param source: the profiling tool source to use. Omission or 'None' implies
        'mprof'. Options are: 'mprof', 'gnu-time' and 'all', which means all.
    @param plot_function: the plot style between 'matplotlib.pyplot.plot' (
        per omission) and 'matplotlib.pyplot.loglog', for a logarithmical
        display
    @param save_flag: set to false (per omission) plots the graphic, set to
        true save the plot to a file.
    @param output_filepath: the relative path and name to the output plot
        file, if using 'save_flag=True'. If it is omitted a file named based
        in the time stamp will be saved in the current directory.
    """
    plt.figure()  # Necessary to refresh if there is a previous plot charged

    input_source = source  # This is a patch to be able to reuse previous code

    for library_name in library_names:
        samples_by_processes, machine_register = \
            get_samples_grouped_by_processes(library_name,
                                             machine_register,
                                             desired_processes)

        # Here is the second part of the patch, to avoid effects of the
        # following overwriting after the first execution
        source = input_source

        # Configure mode
        if source == 'mprof':
            source = get_used_memory_mprof
        elif source == 'gnu-time':
            source = get_used_memory_gnu_time
        elif source == 'all':
            source = None
        else:
            raise ValueError(source)

        # Refine results: mean use of memory
        refined_samples = []
        for i in range(0, len(samples_by_processes)):
            sizes_and_times = []
            try:
                for s in samples_by_processes[i]:
                    if source:
                        try:
                            mem_reg = source(s)
                        except UnavailableReport:
                            continue  # Impossible use this source
                    else:
                        gnu_time_mem_reg = get_used_memory_gnu_time(s)
                        try:
                            # Merge the two register sources into one value
                            mem_reg = statistics.mean([gnu_time_mem_reg,
                                                       get_used_memory_mprof(
                                                           s)])
                        except UnavailableReport:
                            mem_reg = gnu_time_mem_reg

                    sizes_and_times.append({'size': get_mesh_size(s),
                                            'memory': mem_reg})
            except NoisySampleError:
                pass
            sizes = set([s['size'] for s in sizes_and_times])
            refined_samples_label = []
            for size in sizes:
                if size <= desired_max_size:
                    mean_memory = statistics.mean(
                        [s['memory'] for s in sizes_and_times
                         if s['size'] == size])
                    refined_samples_label.append(
                        {'size': size, 'memory': mean_memory})
            refined_samples.append(refined_samples_label)

        # Sort by mesh size
        for samples in refined_samples:
            samples.sort(key=lambda s: s['size'])

        for i in range(0, len(refined_samples)):
            x = [s['size'] for s in refined_samples[i]]
            y = [s['memory'] for s in refined_samples[i]]

            # Use a color for each library
            if library_name == 'fenics':
                color = 'red'
            elif library_name == 'dealii':
                color = 'blue'
            elif library_name == 'oomph-lib':
                color = 'green'
            else:
                color = 'black'

            # # Different markers for odds and evens
            # if not i % 2:  # Odd
            #     marker = 'P'
            # else:  # Even
            #     marker = 'x'

            # Plotting the points
            plot_function(x, y, color=color)  # Add 'marker = marker' to use

    # Decoration
    plt.xlabel('mesh sizes (in points)')
    plt.ylabel('memory used (in MiB)')
    plt.suptitle(
        f'Used memory / mesh size scalability of each library')
    plt.title('\nMachine: ' + get_machine_data_resume(machine_register),
              fontdict={'fontsize': 'small'})
    fenics_legend_label = mpatches.Patch(color='red', label='FEniCS')
    dealii_legend_label = mpatches.Patch(color='blue', label='Deal II')
    oomph_lib_legend_label = mpatches.Patch(color='green', label='Oomph-lib')
    plt.legend(handles=[fenics_legend_label, dealii_legend_label,
                        oomph_lib_legend_label])

    # Show or save to a file the plot
    if save_flag:
        if not output_filepath:
            output_filepath = str(
                time.time_ns())  # Use timestamp to file name
        plt.savefig(output_filepath)
    else:
        plt.show()
