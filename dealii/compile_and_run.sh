#!/bin/bash

###########################################################
##
## Comparative study of C++ libraries for the finite
## element method
##
## Copyright 2020 Borja González Seoane
##
## This script must be run into the built Docker container.
## Its function is to compile and run an essay with the
## specified parameters, saving the results and some system
## information.
## 
## This is the Deal II version.
##
## Use:
## ./compile_and_run.sh PROCESSES SIZE_POWER MESH_SIZE RESULTS_DIR
##
###########################################################

# Start message
echo "Starting essay..."

# Check the environment
if [[ "$#" -eq 4 ]]
then
	PROCESSES=$1
	SIZE_POWER=$2
	MESH_SIZE=$3
	RESULTS_DIR=$4
else
	PROCESSES=1   # Per omission, no parallelism
	SIZE_POWER=5  # Per omission, mesh of 32*32, i.e. (2^5)^2
	MESH_SIZE=32  # Per omission, mesh of 32*32
	RESULTS_DIR=$1
fi

echo "Using $PROCESSES parallel processes with a mesh of $MESH_SIZE*$MESH_SIZE"

# Set up the correct mesh size
sed -i "s/#define SIZE_POWER .*/#define SIZE_POWER $SIZE_POWER/" test.cpp

# Compile the problem solver
cmake .
make

# Run the tests, first using GNU Time and then with M-Prof and Valgrind
# to take measures
echo "RUNNING THE TESTS..."
TIMES_REP_FILE=times-report.txt
MASSIF_REP_FILE=massif-report.txt
CACHEGRIND_REP_FILE=cachegrind-report.txt
if [[ "$PROCESSES" -eq 1 ]]
then
	(/usr/bin/time -v ./dealii-test) 2> $TIMES_REP_FILE
	mprof run --include-children mpirun -np 1 ./dealii-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms mpirun -np 1 ./dealii-test 
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE mpirun -np 1 ./dealii-test
else
	(/usr/bin/time -v mpirun -np $PROCESSES ./dealii-test) 2> $TIMES_REP_FILE
	mprof run --include-children --multiprocess mpirun -np $PROCESSES ./dealii-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms mpirun -np $PROCESSES ./dealii-test
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE mpirun -np $PROCESSES ./dealii-test
fi

# Catch some machine information
LSCPU_REP_FILE=lscpu-report.txt
LSMEM_REP_FILE=lsmem-report.txt
lscpu >> $LSCPU_REP_FILE
lsmem >> $LSMEM_REP_FILE

# Create a output directory to save all the generated stuff
mkdir "$RESULTS_DIR"

# Save the report files
mv $TIMES_REP_FILE "$RESULTS_DIR"
mv $LSCPU_REP_FILE "$RESULTS_DIR"
mv $LSMEM_REP_FILE "$RESULTS_DIR"
mv $MASSIF_REP_FILE "$RESULTS_DIR"
mv $CACHEGRIND_REP_FILE "$RESULTS_DIR"
mv *.dat "$RESULTS_DIR" # M-Prof results

# Final message
echo "Essay finalized..."
echo "**********************************************************"
