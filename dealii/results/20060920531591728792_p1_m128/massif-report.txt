desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 1 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=354
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=358
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=361
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=364
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=366
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=373
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=377
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=8
#-----------
time=380
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=9
#-----------
time=383
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=387
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=392
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=396
mem_heap_B=8799
mem_heap_extra_B=201
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=400
mem_heap_B=8748
mem_heap_extra_B=204
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=14
#-----------
time=403
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=408
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=412
mem_heap_B=9841
mem_heap_extra_B=1303
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=414
mem_heap_B=18134
mem_heap_extra_B=1418
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=417
mem_heap_B=18532
mem_heap_extra_B=1524
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=419
mem_heap_B=18580
mem_heap_extra_B=1532
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=20
#-----------
time=421
mem_heap_B=18628
mem_heap_extra_B=1588
mem_stacks_B=0
heap_tree=detailed
n6: 18628 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n3: 741 0x532C9B8: strdup (strdup.c:42)
  n1: 365 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 365 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 363 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 363 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 363 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 363 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
  n1: 247 0x12D3FF: ??? (in /usr/bin/mpiexec.hydra)
   n2: 247 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 214 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 214 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 214 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 214 0x52B0B95: (below main) (libc-start.c:310)
    n0: 33 in 1 place, below massif's threshold (1.00%)
  n0: 129 in 22 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
 n0: 487 in 19 places, all below massif's threshold (1.00%)
 n1: 264 0x11F0B3: ??? (in /usr/bin/mpiexec.hydra)
  n1: 264 0x11E966: ??? (in /usr/bin/mpiexec.hydra)
   n1: 264 0x11569A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 264 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
     n0: 264 0x52B0B95: (below main) (libc-start.c:310)
#-----------
snapshot=21
#-----------
time=423
mem_heap_B=26731
mem_heap_extra_B=1821
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=425
mem_heap_B=34793
mem_heap_extra_B=2071
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=23
#-----------
time=427
mem_heap_B=42897
mem_heap_extra_B=2183
mem_stacks_B=0
heap_tree=detailed
n8: 42897 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11A49C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x115712: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11BAE5: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11BBCE: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 965 0x532C9B8: strdup (strdup.c:42)
  n0: 965 in 52 places, all below massif's threshold (1.00%)
 n0: 796 in 24 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
#-----------
snapshot=24
#-----------
time=429
mem_heap_B=35081
mem_heap_extra_B=2391
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=431
mem_heap_B=35710
mem_heap_extra_B=2674
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=433
mem_heap_B=35844
mem_heap_extra_B=2924
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=435
mem_heap_B=28361
mem_heap_extra_B=2911
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=437
mem_heap_B=28394
mem_heap_extra_B=2974
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=439
mem_heap_B=28609
mem_heap_extra_B=3359
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=453
mem_heap_B=28593
mem_heap_extra_B=3351
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=455
mem_heap_B=20412
mem_heap_extra_B=2756
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=468
mem_heap_B=20460
mem_heap_extra_B=2764
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=471
mem_heap_B=86107
mem_heap_extra_B=2877
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=528
mem_heap_B=86122
mem_heap_extra_B=2886
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=35
#-----------
time=531
mem_heap_B=610451
mem_heap_extra_B=2925
mem_stacks_B=0
heap_tree=peak
n4: 610451 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4531 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=36
#-----------
time=533
mem_heap_B=610553
mem_heap_extra_B=2903
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=535
mem_heap_B=611661
mem_heap_extra_B=2971
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=38
#-----------
time=538
mem_heap_B=87203
mem_heap_extra_B=2893
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=39
#-----------
time=542
mem_heap_B=87245
mem_heap_extra_B=2923
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=544
mem_heap_B=87203
mem_heap_extra_B=2893
mem_stacks_B=0
heap_tree=detailed
n5: 87203 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2587 in 42 places, all below massif's threshold (1.00%)
 n2: 1888 0x532C9B8: strdup (strdup.c:42)
  n1: 1120 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1120 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1120 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1120 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1120 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
 n1: 1096 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 1096 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1096 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1096 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1096 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 1096 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 1096 0x52B0B95: (below main) (libc-start.c:310)
#-----------
snapshot=41
#-----------
time=719
mem_heap_B=87281
mem_heap_extra_B=2903
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=42
#-----------
time=719
mem_heap_B=87203
mem_heap_extra_B=2893
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=1196
mem_heap_B=87231
mem_heap_extra_B=2905
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=1197
mem_heap_B=87203
mem_heap_extra_B=2893
mem_stacks_B=0
heap_tree=detailed
n5: 87203 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2587 in 42 places, all below massif's threshold (1.00%)
 n2: 1888 0x532C9B8: strdup (strdup.c:42)
  n1: 1120 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1120 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1120 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1120 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1120 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
 n1: 1096 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 1096 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1096 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1096 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1096 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 1096 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 1096 0x52B0B95: (below main) (libc-start.c:310)
#-----------
snapshot=45
#-----------
time=1206
mem_heap_B=87207
mem_heap_extra_B=2913
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=1209
mem_heap_B=151279
mem_heap_extra_B=2817
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=1212
mem_heap_B=216791
mem_heap_extra_B=2777
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=48
#-----------
time=1215
mem_heap_B=216031
mem_heap_extra_B=2129
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=1216
mem_heap_B=214115
mem_heap_extra_B=397
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=1216
mem_heap_B=214097
mem_heap_extra_B=375
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=1216
mem_heap_B=214085
mem_heap_extra_B=363
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=1216
mem_heap_B=214083
mem_heap_extra_B=341
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=53
#-----------
time=1216
mem_heap_B=214069
mem_heap_extra_B=331
mem_stacks_B=0
heap_tree=detailed
n4: 214069 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 1317 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=54
#-----------
time=1216
mem_heap_B=213573
mem_heap_extra_B=323
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=1216
mem_heap_B=213569
mem_heap_extra_B=303
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=1216
mem_heap_B=213565
mem_heap_extra_B=283
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=57
#-----------
time=1216
mem_heap_B=213551
mem_heap_extra_B=273
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=1216
mem_heap_B=213533
mem_heap_extra_B=251
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=1217
mem_heap_B=205485
mem_heap_extra_B=243
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=60
#-----------
time=1217
mem_heap_B=205405
mem_heap_extra_B=235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=1217
mem_heap_B=205391
mem_heap_extra_B=225
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=1217
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=63
#-----------
time=1217
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=detailed
n3: 197325 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 669 in 46 places, all below massif's threshold (1.00%)
#-----------
snapshot=64
#-----------
time=1217
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=65
#-----------
time=1217
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=66
#-----------
time=1217
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=67
#-----------
time=1222
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=68
#-----------
time=1222
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=69
#-----------
time=1222
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=70
#-----------
time=1222
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=71
#-----------
time=1222
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=72
#-----------
time=1222
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=73
#-----------
time=1222
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=detailed
n2: 65984 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 448 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=74
#-----------
time=1222
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=75
#-----------
time=1222
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=76
#-----------
time=1222
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=77
#-----------
time=1222
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=78
#-----------
time=1222
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=79
#-----------
time=1222
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=80
#-----------
time=1222
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
