desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 3 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=353
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=357
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=361
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=364
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=366
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=373
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=377
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=8
#-----------
time=380
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=9
#-----------
time=383
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=387
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=392
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=396
mem_heap_B=8799
mem_heap_extra_B=201
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=400
mem_heap_B=8741
mem_heap_extra_B=187
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=14
#-----------
time=403
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=408
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=412
mem_heap_B=9841
mem_heap_extra_B=1303
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=414
mem_heap_B=18134
mem_heap_extra_B=1418
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=417
mem_heap_B=18556
mem_heap_extra_B=1532
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=421
mem_heap_B=26678
mem_heap_extra_B=1626
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=20
#-----------
time=425
mem_heap_B=34817
mem_heap_extra_B=2079
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=429
mem_heap_B=35090
mem_heap_extra_B=2366
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=431
mem_heap_B=35714
mem_heap_extra_B=2662
mem_stacks_B=0
heap_tree=detailed
n8: 35714 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11A49C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x115712: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11BAE5: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 1131 0x532C9B8: strdup (strdup.c:42)
  n0: 1131 in 68 places, all below massif's threshold (1.00%)
 n0: 768 in 25 places, all below massif's threshold (1.00%)
 n2: 679 0x130CBB: ??? (in /usr/bin/mpiexec.hydra)
  n1: 617 0x12D2CD: ??? (in /usr/bin/mpiexec.hydra)
   n1: 617 0x11C1CF: ??? (in /usr/bin/mpiexec.hydra)
    n1: 617 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 617 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 617 0x52B0B95: (below main) (libc-start.c:310)
  n0: 62 in 6 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
#-----------
snapshot=23
#-----------
time=433
mem_heap_B=35868
mem_heap_extra_B=2932
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=435
mem_heap_B=28385
mem_heap_extra_B=2919
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=437
mem_heap_B=28418
mem_heap_extra_B=2982
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=439
mem_heap_B=28633
mem_heap_extra_B=3367
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=453
mem_heap_B=28617
mem_heap_extra_B=3359
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=455
mem_heap_B=20436
mem_heap_extra_B=2764
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=468
mem_heap_B=20484
mem_heap_extra_B=2772
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=470
mem_heap_B=20659
mem_heap_extra_B=2861
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=472
mem_heap_B=86187
mem_heap_extra_B=2877
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=531
mem_heap_B=86243
mem_heap_extra_B=2893
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=534
mem_heap_B=610647
mem_heap_extra_B=2961
mem_stacks_B=0
heap_tree=peak
n4: 610647 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4727 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=34
#-----------
time=536
mem_heap_B=611597
mem_heap_extra_B=2923
mem_stacks_B=0
heap_tree=detailed
n4: 611597 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 5677 in 41 places, all below massif's threshold (1.00%)
#-----------
snapshot=35
#-----------
time=538
mem_heap_B=611707
mem_heap_extra_B=3037
mem_stacks_B=0
heap_tree=detailed
n4: 611707 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 5787 in 42 places, all below massif's threshold (1.00%)
#-----------
snapshot=36
#-----------
time=540
mem_heap_B=612013
mem_heap_extra_B=2923
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=542
mem_heap_B=90571
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=38
#-----------
time=546
mem_heap_B=90613
mem_heap_extra_B=2971
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=39
#-----------
time=547
mem_heap_B=90571
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=889
mem_heap_B=90650
mem_heap_extra_B=2950
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=41
#-----------
time=889
mem_heap_B=90571
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=42
#-----------
time=1691
mem_heap_B=90599
mem_heap_extra_B=2953
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=1692
mem_heap_B=90571
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=1695
mem_heap_B=90571
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=45
#-----------
time=1710
mem_heap_B=90583
mem_heap_extra_B=2953
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=1712
mem_heap_B=85735
mem_heap_extra_B=2785
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=1714
mem_heap_B=216823
mem_heap_extra_B=2793
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=48
#-----------
time=1717
mem_heap_B=215904
mem_heap_extra_B=2000
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=1718
mem_heap_B=213551
mem_heap_extra_B=273
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=1719
mem_heap_B=213533
mem_heap_extra_B=251
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=1719
mem_heap_B=205485
mem_heap_extra_B=243
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=1719
mem_heap_B=205405
mem_heap_extra_B=235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=53
#-----------
time=1719
mem_heap_B=205391
mem_heap_extra_B=225
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=1719
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=detailed
n4: 205373 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 669 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=55
#-----------
time=1719
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=1719
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=57
#-----------
time=1719
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=1719
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=1725
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=60
#-----------
time=1725
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=1725
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=1725
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=63
#-----------
time=1725
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=64
#-----------
time=1725
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n2: 65992 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 456 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=65
#-----------
time=1725
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=66
#-----------
time=1725
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=67
#-----------
time=1725
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=68
#-----------
time=1725
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=69
#-----------
time=1725
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=70
#-----------
time=1725
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=71
#-----------
time=1725
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=72
#-----------
time=1725
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
