desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 4 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=436
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=441
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=445
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=449
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=452
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=460
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=460
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=8
#-----------
time=465
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=9
#-----------
time=468
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=472
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=477
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=483
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=486
mem_heap_B=8731
mem_heap_extra_B=173
mem_stacks_B=0
heap_tree=detailed
n4: 8731 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n0: 35 in 7 places, all below massif's threshold (1.00%)
#-----------
snapshot=14
#-----------
time=490
mem_heap_B=8847
mem_heap_extra_B=265
mem_stacks_B=0
heap_tree=detailed
n5: 8847 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n1: 107 0x532C9B8: strdup (strdup.c:42)
  n0: 107 in 7 places, all below massif's threshold (1.00%)
 n0: 44 in 8 places, all below massif's threshold (1.00%)
#-----------
snapshot=15
#-----------
time=494
mem_heap_B=12837
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=497
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=503
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=507
mem_heap_B=9438
mem_heap_extra_B=874
mem_stacks_B=0
heap_tree=detailed
n6: 9438 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n3: 375 0x532C9B8: strdup (strdup.c:42)
  n1: 157 0x12D3FF: ??? (in /usr/bin/mpiexec.hydra)
   n2: 157 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 124 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 124 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 124 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 124 0x52B0B95: (below main) (libc-start.c:310)
    n0: 33 in 1 place, below massif's threshold (1.00%)
  n1: 115 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 115 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 113 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 113 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 113 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 113 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
  n0: 103 in 15 places, all below massif's threshold (1.00%)
 n1: 288 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 288 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 264 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 264 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 264 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 264 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n0: 79 in 10 places, all below massif's threshold (1.00%)
#-----------
snapshot=19
#-----------
time=511
mem_heap_B=18147
mem_heap_extra_B=1429
mem_stacks_B=0
heap_tree=detailed
n5: 18147 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n3: 740 0x532C9B8: strdup (strdup.c:42)
  n1: 365 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 365 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 363 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 363 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 363 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 363 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
  n1: 247 0x12D3FF: ??? (in /usr/bin/mpiexec.hydra)
   n2: 247 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 214 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 214 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 214 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 214 0x52B0B95: (below main) (libc-start.c:310)
    n0: 33 in 1 place, below massif's threshold (1.00%)
  n0: 128 in 18 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
 n0: 271 in 12 places, all below massif's threshold (1.00%)
#-----------
snapshot=20
#-----------
time=514
mem_heap_B=18568
mem_heap_extra_B=1520
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=518
mem_heap_B=18639
mem_heap_extra_B=1569
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=522
mem_heap_B=26770
mem_heap_extra_B=1838
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=23
#-----------
time=524
mem_heap_B=34829
mem_heap_extra_B=2067
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=527
mem_heap_B=42933
mem_heap_extra_B=2179
mem_stacks_B=0
heap_tree=detailed
n8: 42933 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11A49C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x115712: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11BAE5: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11BBCE: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n1: 965 0x532C9B8: strdup (strdup.c:42)
  n0: 965 in 52 places, all below massif's threshold (1.00%)
 n0: 832 in 24 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
#-----------
snapshot=25
#-----------
time=531
mem_heap_B=35724
mem_heap_extra_B=2628
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=535
mem_heap_B=36422
mem_heap_extra_B=2994
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=539
mem_heap_B=28572
mem_heap_extra_B=3244
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=541
mem_heap_B=28645
mem_heap_extra_B=3355
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=558
mem_heap_B=28629
mem_heap_extra_B=3347
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=560
mem_heap_B=20448
mem_heap_extra_B=2752
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=575
mem_heap_B=20496
mem_heap_extra_B=2760
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=577
mem_heap_B=20672
mem_heap_extra_B=2816
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=580
mem_heap_B=86227
mem_heap_extra_B=2853
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=647
mem_heap_B=86283
mem_heap_extra_B=2869
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=35
#-----------
time=650
mem_heap_B=610687
mem_heap_extra_B=2953
mem_stacks_B=0
heap_tree=detailed
n4: 610687 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4767 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=36
#-----------
time=654
mem_heap_B=611761
mem_heap_extra_B=2991
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=656
mem_heap_B=611637
mem_heap_extra_B=2899
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=38
#-----------
time=659
mem_heap_B=616923
mem_heap_extra_B=3141
mem_stacks_B=0
heap_tree=peak
n4: 616923 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 11003 in 42 places, all below massif's threshold (1.00%)
#-----------
snapshot=39
#-----------
time=660
mem_heap_B=91707
mem_heap_extra_B=2933
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=665
mem_heap_B=91749
mem_heap_extra_B=2963
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=41
#-----------
time=666
mem_heap_B=91707
mem_heap_extra_B=2933
mem_stacks_B=0
heap_tree=detailed
n5: 91707 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 5480 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 5480 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 5480 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 5480 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 5480 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 5480 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 5480 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2707 in 42 places, all below massif's threshold (1.00%)
 n2: 1888 0x532C9B8: strdup (strdup.c:42)
  n1: 1120 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1120 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1120 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1120 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1120 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
#-----------
snapshot=42
#-----------
time=690
mem_heap_B=91784
mem_heap_extra_B=2944
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=690
mem_heap_B=91707
mem_heap_extra_B=2933
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=739
mem_heap_B=91735
mem_heap_extra_B=2945
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=45
#-----------
time=740
mem_heap_B=91707
mem_heap_extra_B=2933
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=747
mem_heap_B=91723
mem_heap_extra_B=2941
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=751
mem_heap_B=151303
mem_heap_extra_B=2793
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=48
#-----------
time=754
mem_heap_B=216815
mem_heap_extra_B=2753
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=758
mem_heap_B=214170
mem_heap_extra_B=430
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=758
mem_heap_B=214153
mem_heap_extra_B=407
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=758
mem_heap_B=214151
mem_heap_extra_B=385
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=758
mem_heap_B=214139
mem_heap_extra_B=373
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=53
#-----------
time=758
mem_heap_B=214121
mem_heap_extra_B=351
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=758
mem_heap_B=214109
mem_heap_extra_B=339
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=758
mem_heap_B=214107
mem_heap_extra_B=317
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=758
mem_heap_B=214093
mem_heap_extra_B=307
mem_stacks_B=0
heap_tree=detailed
n4: 214093 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 1341 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=57
#-----------
time=758
mem_heap_B=213597
mem_heap_extra_B=299
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=758
mem_heap_B=213581
mem_heap_extra_B=291
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=759
mem_heap_B=213565
mem_heap_extra_B=283
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=60
#-----------
time=759
mem_heap_B=213551
mem_heap_extra_B=273
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=759
mem_heap_B=213533
mem_heap_extra_B=251
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=759
mem_heap_B=205485
mem_heap_extra_B=243
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=63
#-----------
time=759
mem_heap_B=205405
mem_heap_extra_B=235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=64
#-----------
time=759
mem_heap_B=205391
mem_heap_extra_B=225
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=65
#-----------
time=759
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=66
#-----------
time=759
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=detailed
n3: 197325 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 669 in 46 places, all below massif's threshold (1.00%)
#-----------
snapshot=67
#-----------
time=760
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=68
#-----------
time=760
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=69
#-----------
time=760
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=70
#-----------
time=766
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=71
#-----------
time=766
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=72
#-----------
time=766
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=73
#-----------
time=766
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=74
#-----------
time=766
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=75
#-----------
time=766
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=76
#-----------
time=766
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=detailed
n2: 65984 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 448 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=77
#-----------
time=766
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=78
#-----------
time=766
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=79
#-----------
time=766
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=80
#-----------
time=766
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=81
#-----------
time=766
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=82
#-----------
time=766
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=83
#-----------
time=766
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
