desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 5 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=437
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=442
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=446
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=449
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=452
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=460
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=460
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=8
#-----------
time=465
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=9
#-----------
time=469
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=473
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=477
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=484
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=486
mem_heap_B=8731
mem_heap_extra_B=173
mem_stacks_B=0
heap_tree=detailed
n4: 8731 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n0: 35 in 7 places, all below massif's threshold (1.00%)
#-----------
snapshot=14
#-----------
time=489
mem_heap_B=8799
mem_heap_extra_B=201
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=493
mem_heap_B=8824
mem_heap_extra_B=224
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=497
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=504
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=508
mem_heap_B=9570
mem_heap_extra_B=982
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=512
mem_heap_B=18147
mem_heap_extra_B=1429
mem_stacks_B=0
heap_tree=detailed
n5: 18147 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n3: 740 0x532C9B8: strdup (strdup.c:42)
  n1: 365 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 365 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 363 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 363 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 363 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 363 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
  n1: 247 0x12D3FF: ??? (in /usr/bin/mpiexec.hydra)
   n2: 247 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 214 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 214 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 214 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 214 0x52B0B95: (below main) (libc-start.c:310)
    n0: 33 in 1 place, below massif's threshold (1.00%)
  n0: 128 in 18 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
 n0: 271 in 12 places, all below massif's threshold (1.00%)
#-----------
snapshot=20
#-----------
time=514
mem_heap_B=18580
mem_heap_extra_B=1524
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=517
mem_heap_B=18628
mem_heap_extra_B=1532
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=521
mem_heap_B=26749
mem_heap_extra_B=1731
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=23
#-----------
time=525
mem_heap_B=26810
mem_heap_extra_B=1878
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=529
mem_heap_B=35099
mem_heap_extra_B=2277
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=533
mem_heap_B=35819
mem_heap_extra_B=2773
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=537
mem_heap_B=28414
mem_heap_extra_B=2930
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=539
mem_heap_B=28440
mem_heap_extra_B=2952
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=542
mem_heap_B=28658
mem_heap_extra_B=3358
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=558
mem_heap_B=28642
mem_heap_extra_B=3350
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=561
mem_heap_B=20461
mem_heap_extra_B=2755
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=575
mem_heap_B=20509
mem_heap_extra_B=2763
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=577
mem_heap_B=20713
mem_heap_extra_B=2839
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=580
mem_heap_B=86268
mem_heap_extra_B=2876
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=657
mem_heap_B=86324
mem_heap_extra_B=2892
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=35
#-----------
time=660
mem_heap_B=610728
mem_heap_extra_B=2960
mem_stacks_B=0
heap_tree=detailed
n4: 610728 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4808 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=36
#-----------
time=664
mem_heap_B=611802
mem_heap_extra_B=3030
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=667
mem_heap_B=87364
mem_heap_extra_B=2892
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=38
#-----------
time=670
mem_heap_B=618289
mem_heap_extra_B=3223
mem_stacks_B=0
heap_tree=peak
n5: 618289 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 6576 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 6576 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 6576 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 6576 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 6576 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 6576 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 6576 0x52B0B95: (below main) (libc-start.c:310)
 n0: 5793 in 41 places, all below massif's threshold (1.00%)
#-----------
snapshot=39
#-----------
time=671
mem_heap_B=92844
mem_heap_extra_B=2972
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=675
mem_heap_B=92886
mem_heap_extra_B=2986
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=41
#-----------
time=677
mem_heap_B=92844
mem_heap_extra_B=2972
mem_stacks_B=0
heap_tree=detailed
n5: 92844 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 6576 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 6576 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 6576 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 6576 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 6576 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 6576 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 6576 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2747 in 42 places, all below massif's threshold (1.00%)
 n2: 1889 0x532C9B8: strdup (strdup.c:42)
  n1: 1121 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1121 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1121 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1121 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1121 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
#-----------
snapshot=42
#-----------
time=714
mem_heap_B=92921
mem_heap_extra_B=2999
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=714
mem_heap_B=92844
mem_heap_extra_B=2972
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=760
mem_heap_B=92872
mem_heap_extra_B=2984
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=45
#-----------
time=762
mem_heap_B=92844
mem_heap_extra_B=2972
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=769
mem_heap_B=92864
mem_heap_extra_B=2992
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=772
mem_heap_B=151312
mem_heap_extra_B=2816
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=48
#-----------
time=776
mem_heap_B=216768
mem_heap_extra_B=2728
mem_stacks_B=0
heap_tree=detailed
n4: 216768 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4016 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=49
#-----------
time=779
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=detailed
n4: 205373 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 669 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=50
#-----------
time=779
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=779
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=779
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=53
#-----------
time=779
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=784
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=784
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=784
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=57
#-----------
time=784
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=784
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=784
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n2: 65992 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 456 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=60
#-----------
time=784
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=784
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=784
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=63
#-----------
time=784
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=64
#-----------
time=784
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=65
#-----------
time=784
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=66
#-----------
time=784
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=67
#-----------
time=784
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
