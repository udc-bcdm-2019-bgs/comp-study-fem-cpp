desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 6 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=353
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=356
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=360
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=363
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=365
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=372
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=376
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=8
#-----------
time=379
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=9
#-----------
time=382
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=386
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=391
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=393
mem_heap_B=8731
mem_heap_extra_B=173
mem_stacks_B=0
heap_tree=detailed
n4: 8731 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n0: 35 in 7 places, all below massif's threshold (1.00%)
#-----------
snapshot=13
#-----------
time=396
mem_heap_B=8847
mem_heap_extra_B=265
mem_stacks_B=0
heap_tree=detailed
n5: 8847 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 8048 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n5: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n1: 128 0x568315F: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x568316B: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832C8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n1: 128 0x56832E8: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 128 0x40106EA: _dl_init (dl-init.c:58)
    n1: 128 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 128 0x2: ???
      n1: 128 0x1FFF000D81: ???
       n1: 128 0x1FFF000D88: ???
        n1: 128 0x1FFF000D8C: ???
         n0: 128 0x1FFF000D8E: ???
  n0: 24 in 4 places, all below massif's threshold (1.00%)
 n1: 112 0x5683A34: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 112 in 8 places, all below massif's threshold (1.00%)
 n1: 107 0x532C9B8: strdup (strdup.c:42)
  n0: 107 in 7 places, all below massif's threshold (1.00%)
 n0: 44 in 8 places, all below massif's threshold (1.00%)
#-----------
snapshot=14
#-----------
time=400
mem_heap_B=8759
mem_heap_extra_B=209
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=402
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=407
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=409
mem_heap_B=8939
mem_heap_extra_B=349
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=412
mem_heap_B=18134
mem_heap_extra_B=1418
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=416
mem_heap_B=18592
mem_heap_extra_B=1528
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=20
#-----------
time=420
mem_heap_B=26722
mem_heap_extra_B=1630
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=424
mem_heap_B=34872
mem_heap_extra_B=2120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=428
mem_heap_B=35142
mem_heap_extra_B=2394
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=23
#-----------
time=432
mem_heap_B=35893
mem_heap_extra_B=2915
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=436
mem_heap_B=28452
mem_heap_extra_B=2956
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=439
mem_heap_B=28670
mem_heap_extra_B=3362
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=452
mem_heap_B=28654
mem_heap_extra_B=3354
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=455
mem_heap_B=20473
mem_heap_extra_B=2759
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=467
mem_heap_B=20521
mem_heap_extra_B=2767
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=469
mem_heap_B=20753
mem_heap_extra_B=2831
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=471
mem_heap_B=86308
mem_heap_extra_B=2868
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=555
mem_heap_B=86364
mem_heap_extra_B=2884
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=558
mem_heap_B=610768
mem_heap_extra_B=2952
mem_stacks_B=0
heap_tree=detailed
n4: 610768 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4848 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=33
#-----------
time=560
mem_heap_B=611800
mem_heap_extra_B=2936
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=562
mem_heap_B=611842
mem_heap_extra_B=3006
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=35
#-----------
time=565
mem_heap_B=87404
mem_heap_extra_B=2884
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=36
#-----------
time=568
mem_heap_B=87838
mem_heap_extra_B=2906
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=569
mem_heap_B=619654
mem_heap_extra_B=3258
mem_stacks_B=0
heap_tree=peak
n5: 619654 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 7672 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 7672 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 7672 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 7672 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 7672 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 7672 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 7672 0x52B0B95: (below main) (libc-start.c:310)
 n0: 6062 in 41 places, all below massif's threshold (1.00%)
#-----------
snapshot=38
#-----------
time=570
mem_heap_B=93980
mem_heap_extra_B=2980
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=39
#-----------
time=577
mem_heap_B=94022
mem_heap_extra_B=2994
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=578
mem_heap_B=93980
mem_heap_extra_B=2980
mem_stacks_B=0
heap_tree=detailed
n5: 93980 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 7672 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 7672 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 7672 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 7672 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 7672 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 7672 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 7672 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2787 in 42 places, all below massif's threshold (1.00%)
 n2: 1889 0x532C9B8: strdup (strdup.c:42)
  n1: 1121 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1121 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1121 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1121 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1121 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
#-----------
snapshot=41
#-----------
time=794
mem_heap_B=94059
mem_heap_extra_B=2989
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=42
#-----------
time=794
mem_heap_B=93980
mem_heap_extra_B=2980
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=1317
mem_heap_B=94008
mem_heap_extra_B=2992
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=1320
mem_heap_B=93980
mem_heap_extra_B=2980
mem_stacks_B=0
heap_tree=detailed
n5: 93980 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 7672 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 7672 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 7672 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 7672 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 7672 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 7672 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 7672 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2787 in 42 places, all below massif's threshold (1.00%)
 n2: 1889 0x532C9B8: strdup (strdup.c:42)
  n1: 1121 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1121 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1121 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1121 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1121 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
#-----------
snapshot=45
#-----------
time=1333
mem_heap_B=94004
mem_heap_extra_B=2996
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=1335
mem_heap_B=93720
mem_heap_extra_B=2912
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=1337
mem_heap_B=216848
mem_heap_extra_B=2800
mem_stacks_B=0
heap_tree=detailed
n4: 216848 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4096 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=48
#-----------
time=1340
mem_heap_B=216171
mem_heap_extra_B=2205
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=1341
mem_heap_B=214206
mem_heap_extra_B=490
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=1341
mem_heap_B=214188
mem_heap_extra_B=468
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=1341
mem_heap_B=214186
mem_heap_extra_B=446
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=1341
mem_heap_B=214169
mem_heap_extra_B=423
mem_stacks_B=0
heap_tree=detailed
n4: 214169 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 1417 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=53
#-----------
time=1341
mem_heap_B=214167
mem_heap_extra_B=401
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=1341
mem_heap_B=214155
mem_heap_extra_B=389
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=1341
mem_heap_B=214137
mem_heap_extra_B=367
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=1341
mem_heap_B=214125
mem_heap_extra_B=355
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=57
#-----------
time=1341
mem_heap_B=214123
mem_heap_extra_B=333
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=1341
mem_heap_B=214109
mem_heap_extra_B=323
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=1341
mem_heap_B=213613
mem_heap_extra_B=315
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=60
#-----------
time=1341
mem_heap_B=213589
mem_heap_extra_B=299
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=1341
mem_heap_B=213565
mem_heap_extra_B=283
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=1341
mem_heap_B=213551
mem_heap_extra_B=273
mem_stacks_B=0
heap_tree=detailed
n4: 213551 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 799 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=63
#-----------
time=1341
mem_heap_B=213533
mem_heap_extra_B=251
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=64
#-----------
time=1341
mem_heap_B=205485
mem_heap_extra_B=243
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=65
#-----------
time=1341
mem_heap_B=205405
mem_heap_extra_B=235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=66
#-----------
time=1342
mem_heap_B=205391
mem_heap_extra_B=225
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=67
#-----------
time=1342
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=68
#-----------
time=1342
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=69
#-----------
time=1342
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=70
#-----------
time=1342
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=71
#-----------
time=1342
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=72
#-----------
time=1346
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=detailed
n2: 66176 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 640 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=73
#-----------
time=1346
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=74
#-----------
time=1346
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=75
#-----------
time=1346
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=76
#-----------
time=1346
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=77
#-----------
time=1346
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=78
#-----------
time=1346
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=79
#-----------
time=1346
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=80
#-----------
time=1346
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=81
#-----------
time=1346
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=82
#-----------
time=1346
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=detailed
n2: 65696 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 160 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=83
#-----------
time=1346
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=84
#-----------
time=1346
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=85
#-----------
time=1346
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
