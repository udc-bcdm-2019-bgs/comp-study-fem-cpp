desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 7 ./dealii-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=437
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=441
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=446
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=449
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=452
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=460
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=460
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=8
#-----------
time=465
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000D81: ???
       n1: 32816 0x1FFF000D88: ???
        n1: 32816 0x1FFF000D8C: ???
         n0: 32816 0x1FFF000D8E: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000D81: ???
           n1: 4096 0x1FFF000D88: ???
            n1: 4096 0x1FFF000D8C: ???
             n0: 4096 0x1FFF000D8E: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000D81: ???
        n1: 552 0x1FFF000D88: ???
         n1: 552 0x1FFF000D8C: ???
          n0: 552 0x1FFF000D8E: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=9
#-----------
time=469
mem_heap_B=2344
mem_heap_extra_B=176
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=473
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=477
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=484
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=488
mem_heap_B=8764
mem_heap_extra_B=196
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=14
#-----------
time=492
mem_heap_B=8799
mem_heap_extra_B=201
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=494
mem_heap_B=12837
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=497
mem_heap_B=8856
mem_heap_extra_B=312
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=503
mem_heap_B=8904
mem_heap_extra_B=320
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=506
mem_heap_B=8939
mem_heap_extra_B=349
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=510
mem_heap_B=18134
mem_heap_extra_B=1418
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=20
#-----------
time=514
mem_heap_B=18604
mem_heap_extra_B=1532
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=517
mem_heap_B=18652
mem_heap_extra_B=1540
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=521
mem_heap_B=26780
mem_heap_extra_B=1756
mem_stacks_B=0
heap_tree=detailed
n6: 26780 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11A49C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x115712: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n0: 823 in 20 places, all below massif's threshold (1.00%)
 n2: 821 0x532C9B8: strdup (strdup.c:42)
  n0: 456 in 32 places, all below massif's threshold (1.00%)
  n1: 365 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 365 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 363 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 363 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 363 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 363 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 504 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 504 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 480 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 480 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 480 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 480 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
#-----------
snapshot=23
#-----------
time=525
mem_heap_B=26834
mem_heap_extra_B=1886
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=529
mem_heap_B=35126
mem_heap_extra_B=2306
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=533
mem_heap_B=35843
mem_heap_extra_B=2781
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=26
#-----------
time=537
mem_heap_B=28438
mem_heap_extra_B=2938
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=539
mem_heap_B=28464
mem_heap_extra_B=2960
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=28
#-----------
time=542
mem_heap_B=28682
mem_heap_extra_B=3366
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=558
mem_heap_B=28666
mem_heap_extra_B=3358
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=561
mem_heap_B=20485
mem_heap_extra_B=2763
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=31
#-----------
time=576
mem_heap_B=20533
mem_heap_extra_B=2771
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=580
mem_heap_B=86348
mem_heap_extra_B=2876
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=681
mem_heap_B=86404
mem_heap_extra_B=2892
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=684
mem_heap_B=610808
mem_heap_extra_B=2960
mem_stacks_B=0
heap_tree=detailed
n4: 610808 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 4888 in 39 places, all below massif's threshold (1.00%)
#-----------
snapshot=35
#-----------
time=688
mem_heap_B=611953
mem_heap_extra_B=3095
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=36
#-----------
time=692
mem_heap_B=87949
mem_heap_extra_B=2907
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=694
mem_heap_B=621019
mem_heap_extra_B=3293
mem_stacks_B=0
heap_tree=peak
n5: 621019 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 524288 0x117F90: ??? (in /usr/bin/mpiexec.hydra)
  n1: 524288 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 524288 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 524288 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 524288 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8768 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8768 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8768 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8768 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8768 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8768 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 8768 0x52B0B95: (below main) (libc-start.c:310)
 n0: 6331 in 41 places, all below massif's threshold (1.00%)
#-----------
snapshot=38
#-----------
time=695
mem_heap_B=95116
mem_heap_extra_B=3004
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=39
#-----------
time=700
mem_heap_B=95158
mem_heap_extra_B=3018
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=702
mem_heap_B=95116
mem_heap_extra_B=3004
mem_stacks_B=0
heap_tree=detailed
n5: 95116 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8768 0x11F1FC: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8768 0x123287: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8768 0x118060: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8768 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8768 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8768 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
       n0: 8768 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2827 in 42 places, all below massif's threshold (1.00%)
 n2: 1889 0x532C9B8: strdup (strdup.c:42)
  n1: 1121 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 1121 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 1121 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 1121 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 1121 0x52B0B95: (below main) (libc-start.c:310)
  n0: 768 in 97 places, all below massif's threshold (1.00%)
#-----------
snapshot=41
#-----------
time=736
mem_heap_B=95193
mem_heap_extra_B=3015
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=42
#-----------
time=736
mem_heap_B=95116
mem_heap_extra_B=3004
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=791
mem_heap_B=95144
mem_heap_extra_B=3016
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=792
mem_heap_B=95116
mem_heap_extra_B=3004
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=45
#-----------
time=801
mem_heap_B=95144
mem_heap_extra_B=3016
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=46
#-----------
time=804
mem_heap_B=151328
mem_heap_extra_B=2800
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=806
mem_heap_B=216888
mem_heap_extra_B=2816
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=48
#-----------
time=809
mem_heap_B=216189
mem_heap_extra_B=2235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=810
mem_heap_B=214230
mem_heap_extra_B=514
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=811
mem_heap_B=214216
mem_heap_extra_B=504
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=811
mem_heap_B=214214
mem_heap_extra_B=482
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=811
mem_heap_B=214196
mem_heap_extra_B=460
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=53
#-----------
time=811
mem_heap_B=214194
mem_heap_extra_B=438
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=811
mem_heap_B=214177
mem_heap_extra_B=415
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=811
mem_heap_B=214175
mem_heap_extra_B=393
mem_stacks_B=0
heap_tree=detailed
n4: 214175 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 1423 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=56
#-----------
time=811
mem_heap_B=214163
mem_heap_extra_B=381
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=57
#-----------
time=811
mem_heap_B=214145
mem_heap_extra_B=359
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=58
#-----------
time=811
mem_heap_B=214133
mem_heap_extra_B=347
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=59
#-----------
time=811
mem_heap_B=214131
mem_heap_extra_B=325
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=60
#-----------
time=811
mem_heap_B=214117
mem_heap_extra_B=315
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=61
#-----------
time=811
mem_heap_B=213621
mem_heap_extra_B=307
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=62
#-----------
time=811
mem_heap_B=213593
mem_heap_extra_B=295
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=63
#-----------
time=811
mem_heap_B=213565
mem_heap_extra_B=283
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=64
#-----------
time=811
mem_heap_B=213551
mem_heap_extra_B=273
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=65
#-----------
time=811
mem_heap_B=213533
mem_heap_extra_B=251
mem_stacks_B=0
heap_tree=detailed
n4: 213533 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 781 in 45 places, all below massif's threshold (1.00%)
#-----------
snapshot=66
#-----------
time=811
mem_heap_B=205485
mem_heap_extra_B=243
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=67
#-----------
time=811
mem_heap_B=205405
mem_heap_extra_B=235
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=68
#-----------
time=812
mem_heap_B=205391
mem_heap_extra_B=225
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=69
#-----------
time=812
mem_heap_B=205373
mem_heap_extra_B=203
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=70
#-----------
time=812
mem_heap_B=197325
mem_heap_extra_B=195
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=71
#-----------
time=812
mem_heap_B=131765
mem_heap_extra_B=179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=72
#-----------
time=812
mem_heap_B=66205
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=73
#-----------
time=812
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=74
#-----------
time=817
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=75
#-----------
time=817
mem_heap_B=66160
mem_heap_extra_B=120
mem_stacks_B=0
heap_tree=detailed
n2: 66160 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 624 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=76
#-----------
time=817
mem_heap_B=66152
mem_heap_extra_B=104
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=77
#-----------
time=817
mem_heap_B=66136
mem_heap_extra_B=96
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=78
#-----------
time=817
mem_heap_B=66008
mem_heap_extra_B=88
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=79
#-----------
time=817
mem_heap_B=65992
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=80
#-----------
time=817
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=81
#-----------
time=817
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=82
#-----------
time=817
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=83
#-----------
time=817
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=84
#-----------
time=817
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=85
#-----------
time=817
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=detailed
n2: 65680 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 144 in 47 places, all below massif's threshold (1.00%)
#-----------
snapshot=86
#-----------
time=817
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=87
#-----------
time=817
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
