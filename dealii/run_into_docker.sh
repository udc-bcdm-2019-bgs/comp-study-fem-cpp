#!/bin/bash

###########################################################
##
## Comparative study of C++ libraries for the finite
## element method
##
## Copyright 2020 Borja González Seoane
##
## This auxiliary script run into the proper Docker
## container a testing routine for this study.
## 
## This is the Deal II version.
##
##
## To run a experiment, first assure that you have Docker running and the Docker
## image for this library pulled ('udcbcdm2019bgs/dealii'). Then use the
## following command:
## 
## ./run_into_docker.sh
## 
## With this, you run a test in a Docker container using no parallelism and a
## mesh of 32*32.
## 
## To customize this use:
## 
## ./run_into_docker.sh P M
## 
## Where 'P' is the number of parallel processes to use and 'M' the mesh size.
## For example:
## 
## ./run_into_docker.sh 2 128
## 
## You can only use a batch mode execution, with a list of pairs of 'P' and 'M'
## like:
## 
## ./run_into_docker.sh 1 32 2 32 3 32 1 64 2 64 3 64 1 128 2 128 3 128
## 
## With this, you produce the complete list of experiments reusing the same
## Docker container, so its a better approach that using Bash pipes or
## alternative constructions with the script.
##
###########################################################

CONTAINER_ID="$(docker run -v $(pwd)/results:/home/results -it -d udcbcdm2019bgs/dealii /bin/bash)"

# Catch the user credentials to give permissions to result data
HOST_USER="$(id -u)"
HOST_GROUP="$(id -g)"

# Catch the verbosity flag
if [[ "$1" -eq "-v" ]]
then
	VERBOSITY=1
	shift 1
else
	VERBOSITY=0
fi

# If there are not args, call the run script to start a test with the per
# omission configuration, in another case run this script but with all the
# passed configurations.
if [[ "$#" -eq 0 ]]
then
	PROCESSES=1   # Per omission, no parallelism
	SIZE_POWER=5  # Per omission, mesh of 32*32
	# Calculate the mesh size using the input size power
	MESH_SIZE=$((2 ** $SIZE_POWER))
	# Generate a folder hash based on the current time
	RESULTS_DIR="$(date +%y%m%d%H%M%s)_p${PROCESSES}_m${MESH_SIZE}"
	docker exec $CONTAINER_ID sh -c "./compile_and_run.sh $RESULTS_DIR"

	# Copy the output to the shared results folder. It is necessary to run as root
	docker exec -u 0 $CONTAINER_ID sh -c "cp -r /home/worker/work/$RESULTS_DIR /home/results/"
	docker exec -u 0 $CONTAINER_ID sh -c "chown -R $HOST_USER:$HOST_GROUP /home/results"
else
	while (( "$#" >= 2 ))
	do
		PROCESSES=$1
		SIZE_POWER=$2
		# Calculate the mesh size using the input size power
		MESH_SIZE=$((2 ** $SIZE_POWER))
		# Generate a folder hash based on the current time
		RESULTS_DIR="$(date +%y%m%d%H%M%s)_p${PROCESSES}_m${MESH_SIZE}"
	    docker exec $CONTAINER_ID sh -c "./compile_and_run.sh $PROCESSES $SIZE_POWER $MESH_SIZE $RESULTS_DIR"
	    shift 2

		# Copy the output to the shared results folder. It is necessary to run as root
		docker exec -u 0 $CONTAINER_ID sh -c "cp -r /home/worker/work/$RESULTS_DIR /home/results/"
		docker exec -u 0 $CONTAINER_ID sh -c "chown -R $HOST_USER:$HOST_GROUP /home/results"
	done
fi

# Kill the running container
docker kill $CONTAINER_ID
docker rm $CONTAINER_ID
