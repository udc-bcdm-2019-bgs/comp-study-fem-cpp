# FeniCS C++ library

## Prepare Docker image

Download the Docker image:

```sh
docker pull udcbcdm2019bgs/fenics:latest
```

## Run an experiment

To run a experiment, first assure that you have Docker running and the Docker image for this library pulled. Then use the following command:

```sh
./run_into_docker.sh
```

With this, you run a test in a Docker container using no parallelism and a mesh of 32\*32.

To customize this use:

```sh
./run_into_docker.sh P M
```

Where `P` is the number of parallel processes to use and `M` the mesh size. For example:

```sh
./run_into_docker.sh 2 128
```

You can only use a batch mode execution, with a list of pairs of `P` and `M` like:

```sh
./run_into_docker.sh 1 32 2 32 3 32 1 64 2 64 3 64 1 128 2 128 3 128
```

With this, you produce the complete list of experiments reusing the same Docker container, so its a better approach that using Bash pipes or alternative constructions with the script.

All the experiment results will be saved into the `results` directory with a concrete format. The subdirectory name is like `20021818191582049976_p1_m32`, where the first part is a hash composed with the current date when the experiment is launched and the following parts `p1` and `m32` denotes that in the experiment was used a process and a mesh of 32.

Inside these subdirectories will be saved all the interesting data about the experiment: FEM results, time and performance measures and machine-about reports.
