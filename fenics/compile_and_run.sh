#!/bin/bash

###########################################################
##
## Comparative study of C++ libraries for the finite
## element method
##
## Copyright 2020 Borja González Seoane
##
## This script must be run into the built Docker container.
## Its function is to compile and run an essay with the
## specified parameters, saving the results and some system
## information.
## 
## This is the FEniCS version.
##
## Use:
## ./compile_and_run.sh PROCESSES SIZE_POWER MESH_SIZE RESULTS_DIR VERBOSITY
##
###########################################################

# Start message
echo "Starting essay..."

# Check the environment
if [[ "$#" -eq 4 ]]
then
	PROCESSES=$1
	MESH_SIZE=$2
	RESULTS_DIR=$3
	VERBOSITY=$4
else
	PROCESSES=1   # Per omission, no parallelism
	MESH_SIZE=32  # Per omission, mesh of 32*32
	RESULTS_DIR=$1
	VERBOSITY=$2
fi

echo "Using $PROCESSES parallel processes with a mesh of $MESH_SIZE*$MESH_SIZE"

# Set up the correct mesh size
sed -i "s/#define MESH_SIZE .*/#define MESH_SIZE $MESH_SIZE/" test.cpp

# Compile the problem solver
cmake .
make

# Run the tests, first using GNU Time and then with M-Prof and Valgrind
# to take measures
echo "RUNNING THE TESTS..."
TIMES_REP_FILE=times-report.txt
MASSIF_REP_FILE=massif-report.txt
CACHEGRIND_REP_FILE=cachegrind-report.txt
if [[ "$PROCESSES" -eq 1 ]]
then
	(/usr/bin/time -v ./fenics-test) 2> $TIMES_REP_FILE
	mprof run ./fenics-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms ./fenics-test 
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE ./fenics-test
else
	(/usr/bin/time -v mpirun -np $PROCESSES ./fenics-test) 2> $TIMES_REP_FILE
	mprof run --include-children --multiprocess mpirun -np $PROCESSES ./fenics-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms mpirun -np $PROCESSES ./fenics-test
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE mpirun -np $PROCESSES ./fenics-test
fi

# Catch some machine information
LSCPU_REP_FILE=lscpu-report.txt
LSMEM_REP_FILE=lsmem-report.txt
lscpu >> $LSCPU_REP_FILE
lsmem >> $LSMEM_REP_FILE

# Create a output directory to save all the generated stuff
mkdir "$RESULTS_DIR"

# Save the result files, if necessary
if [[ "$VERBOSITY" -eq 1 ]]
then
	mv *.pvd "$RESULTS_DIR"
	mv *.vtu "$RESULTS_DIR"
	if test -f *.pvtu
	then  # Not generated in sequential executions
	    mv *.pvtu "$RESULTS_DIR"
	fi
fi

# Save the report files
mv $TIMES_REP_FILE "$RESULTS_DIR"
mv $LSCPU_REP_FILE "$RESULTS_DIR"
mv $LSMEM_REP_FILE "$RESULTS_DIR"
mv $MASSIF_REP_FILE "$RESULTS_DIR"
mv $CACHEGRIND_REP_FILE "$RESULTS_DIR"
mv *.dat "$RESULTS_DIR" # M-Prof results

# Final message
echo "Essay finalized..."
echo "**********************************************************"
