#!/bin/bash

###########################################################
##
## Comparative study of C++ libraries for the finite
## element method
##
## Copyright 2020 Borja González Seoane
##
## This script must be run into the built Docker container.
## Its function is to compile and run an essay with the
## specified parameters, saving the results and some system
## information.
## 
## This is the Oomph-lib version.
##
## Use:
## ./compile_and_run.sh PROCESSES SIZE_POWER MESH_SIZE RESULTS_DIR VERBOSITY
##
###########################################################

# Start message
echo "Starting essay..."

# Check the environment
if [[ "$#" -eq 4 ]]
then
	PROCESSES=$1
	MESH_SIZE=$2
	RESULTS_DIR=$3
	VERBOSITY=$4
else
	PROCESSES=1   # Per omission, no parallelism
	MESH_SIZE=32  # Per omission, mesh of 32*32
	RESULTS_DIR=$1
	VERBOSITY=$2
fi

echo "Using $PROCESSES parallel processes with a mesh of $MESH_SIZE*$MESH_SIZE"

# Set up the correct mesh size
sed -i "s/#define MESH_SIZE .*/#define MESH_SIZE $MESH_SIZE/" test.cpp

# Compile the problem solver
g++ -DHAVE_CONFIG_H -I. -I../..  -isystem /home/worker/oomph-lib-1.0.1306/external_distributions/hypre/hypre_default_installation/include -DOOMPH_HAS_HYPRE -DOOMPH_HAS_STACKTRACE -DOOMPH_HAS_UNISTDH -DOOMPH_HAS_FPUCONTROLH -DOOMPH_HAS_MALLOCH -DOOMPH_HAS_TRIANGLE_LIB -DOOMPH_HAS_TETGEN_LIB -DUSING_OOMPH_SUPERLU -DUSING_OOMPH_SUPERLU_DIST -I/home/worker/oomph-lib-1.0.1306/build/include -I/home/worker/oomph-lib-1.0.1306/build/include -I/home/worker/oomph-lib-1.0.1306/build/include/generic  -DgFortran -O3 -Wall -MT oomph-test.o -MD -MP -MF .deps/test.Tpo -c -o oomph-test.o test.cpp
/bin/bash ../../libtool  --tag=CXX   --mode=link g++ -DgFortran -O3 -Wall -L/home/worker/oomph-lib-1.0.1306/external_distributions/hypre/hypre_default_installation/lib  -o oomph-test oomph-test.o -L/home/worker/oomph-lib-1.0.1306/build/lib -lpoisson -lgeneric -lHYPRE -loomph_hsl -loomph_arpack -loomph_crbond_bessel -loomph_triangle -loomph_tetgen -loomph_superlu_4.3 -loomph_metis_from_parmetis_3.1.1 -loomph_lapack -loomph_flapack -loomph_blas -L/usr/lib/gcc/x86_64-linux-gnu/7 -L/usr/lib/gcc/x86_64-linux-gnu/7/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/7/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/7/../../.. -lgfortran -lm -lquadmath

# Run the tests, first using GNU Time and then with M-Prof and Valgrind
# to take measures
echo "RUNNING THE TESTS..."
TIMES_REP_FILE=times-report.txt
MASSIF_REP_FILE=massif-report.txt
CACHEGRIND_REP_FILE=cachegrind-report.txt
if [[ "$PROCESSES" -eq 1 ]]
then
	(/usr/bin/time -v ./oomph-test) 2> $TIMES_REP_FILE
	mprof run ./oomph-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms ./oomph-test 
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE ./oomph-test
else
	(/usr/bin/time -v mpirun -np $PROCESSES ./oomph-test) 2> $TIMES_REP_FILE
	mprof run --include-children --multiprocess mpirun -np $PROCESSES ./oomph-test
	mprof list
	valgrind --tool=massif --massif-out-file=$MASSIF_REP_FILE --time-unit=ms mpirun -np $PROCESSES ./oomph-test
	valgrind --tool=cachegrind --cachegrind-out-file=$CACHEGRIND_REP_FILE mpirun -np $PROCESSES ./oomph-test
fi

# Catch some machine information
LSCPU_REP_FILE=lscpu-report.txt
LSMEM_REP_FILE=lsmem-report.txt
lscpu >> $LSCPU_REP_FILE
lsmem >> $LSMEM_REP_FILE

# Create a output directory to save all the generated stuff
mkdir "$RESULTS_DIR"

# Save the report files
mv $TIMES_REP_FILE "$RESULTS_DIR"
mv $LSCPU_REP_FILE "$RESULTS_DIR"
mv $LSMEM_REP_FILE "$RESULTS_DIR"
mv $MASSIF_REP_FILE "$RESULTS_DIR"
mv $CACHEGRIND_REP_FILE "$RESULTS_DIR"
mv *.dat "$RESULTS_DIR" # M-Prof results

# Final message
echo "Essay finalized..."
echo "**********************************************************"
