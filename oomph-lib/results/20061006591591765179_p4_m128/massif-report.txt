desc: --massif-out-file=massif-report.txt --time-unit=ms
cmd: mpirun -np 4 ./oomph-test
time_unit: ms
#-----------
snapshot=0
#-----------
time=0
mem_heap_B=0
mem_heap_extra_B=0
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=1
#-----------
time=356
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=2
#-----------
time=360
mem_heap_B=1696
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=3
#-----------
time=364
mem_heap_B=1879
mem_heap_extra_B=33
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=4
#-----------
time=366
mem_heap_B=552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=5
#-----------
time=369
mem_heap_B=33104
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=6
#-----------
time=375
mem_heap_B=33656
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=7
#-----------
time=375
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=8
#-----------
time=380
mem_heap_B=37872
mem_heap_extra_B=80
mem_stacks_B=0
heap_tree=detailed
n4: 37872 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 32816 0x536E9B4: opendir (opendir.c:216)
  n1: 32816 0x5683181: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 32816 0x40106EA: _dl_init (dl-init.c:58)
    n1: 32816 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
     n1: 32816 0x2: ???
      n1: 32816 0x1FFF000E49: ???
       n1: 32816 0x1FFF000E50: ???
        n1: 32816 0x1FFF000E54: ???
         n0: 32816 0x1FFF000E56: ???
 n1: 4096 0x530D18A: _IO_file_doallocate (filedoalloc.c:101)
  n1: 4096 0x531D377: _IO_doallocbuf (genops.c:365)
   n1: 4096 0x531C252: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:495)
    n2: 4096 0x530ED46: getdelim (iogetdelim.c:73)
     n1: 4096 0x5683F09: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
      n1: 4096 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
       n1: 4096 0x40106EA: _dl_init (dl-init.c:58)
        n1: 4096 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
         n1: 4096 0x2: ???
          n1: 4096 0x1FFF000E49: ???
           n1: 4096 0x1FFF000E50: ???
            n1: 4096 0x1FFF000E54: ???
             n0: 4096 0x1FFF000E56: ???
     n0: 0 in 1 place, below massif's threshold (1.00%)
 n2: 552 0x530DE48: fopen@@GLIBC_2.2.5 (iofopen.c:65)
  n1: 552 0x5683EC1: numa_node_size64 (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
   n1: 552 0x56831F3: ??? (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
    n1: 552 0x40106EA: _dl_init (dl-init.c:58)
     n1: 552 0x40010C8: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
      n1: 552 0x2: ???
       n1: 552 0x1FFF000E49: ???
        n1: 552 0x1FFF000E50: ???
         n1: 552 0x1FFF000E54: ???
          n0: 552 0x1FFF000E56: ???
  n0: 0 in 1 place, below massif's threshold (1.00%)
 n0: 408 in 4 places, all below massif's threshold (1.00%)
#-----------
snapshot=9
#-----------
time=384
mem_heap_B=2527
mem_heap_extra_B=169
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=10
#-----------
time=386
mem_heap_B=648
mem_heap_extra_B=136
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=11
#-----------
time=390
mem_heap_B=669
mem_heap_extra_B=155
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=12
#-----------
time=395
mem_heap_B=8717
mem_heap_extra_B=163
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=13
#-----------
time=399
mem_heap_B=8798
mem_heap_extra_B=202
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=14
#-----------
time=403
mem_heap_B=8740
mem_heap_extra_B=188
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=15
#-----------
time=406
mem_heap_B=8897
mem_heap_extra_B=303
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=16
#-----------
time=411
mem_heap_B=8945
mem_heap_extra_B=311
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=17
#-----------
time=415
mem_heap_B=9656
mem_heap_extra_B=832
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=18
#-----------
time=419
mem_heap_B=18211
mem_heap_extra_B=965
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=19
#-----------
time=423
mem_heap_B=26282
mem_heap_extra_B=1022
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=20
#-----------
time=427
mem_heap_B=34464
mem_heap_extra_B=1424
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=21
#-----------
time=431
mem_heap_B=34729
mem_heap_extra_B=1719
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=22
#-----------
time=435
mem_heap_B=35376
mem_heap_extra_B=2240
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=23
#-----------
time=439
mem_heap_B=27794
mem_heap_extra_B=2270
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=24
#-----------
time=442
mem_heap_B=28012
mem_heap_extra_B=2676
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=25
#-----------
time=455
mem_heap_B=27996
mem_heap_extra_B=2668
mem_stacks_B=0
heap_tree=detailed
n6: 27996 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n1: 8000 0x11A49C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8000 0x115712: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8000 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
    n0: 8000 0x52B0B95: (below main) (libc-start.c:310)
 n2: 1839 0x532C9B8: strdup (strdup.c:42)
  n1: 942 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 942 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 942 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 942 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 942 0x52B0B95: (below main) (libc-start.c:310)
  n0: 897 in 85 places, all below massif's threshold (1.00%)
 n0: 1109 in 31 places, all below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n1: 416 0x13138A: ??? (in /usr/bin/mpiexec.hydra)
  n1: 416 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 416 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 416 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
     n0: 416 0x52B0B95: (below main) (libc-start.c:310)
#-----------
snapshot=26
#-----------
time=457
mem_heap_B=19833
mem_heap_extra_B=2055
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=27
#-----------
time=470
mem_heap_B=19881
mem_heap_extra_B=2063
mem_stacks_B=0
heap_tree=detailed
n8: 19881 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n3: 1566 0x532C9B8: strdup (strdup.c:42)
  n1: 942 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 942 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 942 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 942 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 942 0x52B0B95: (below main) (libc-start.c:310)
  n0: 338 in 84 places, all below massif's threshold (1.00%)
  n2: 286 0x12D411: ??? (in /usr/bin/mpiexec.hydra)
   n2: 286 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 284 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
     n1: 284 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
      n1: 284 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
       n0: 284 0x52B0B95: (below main) (libc-start.c:310)
    n0: 2 in 1 place, below massif's threshold (1.00%)
   n0: 0 in 1 place, below massif's threshold (1.00%)
 n1: 536 0x5683A51: numa_bitmask_alloc (in /usr/lib/x86_64-linux-gnu/libnuma.so.1.0.0)
  n0: 536 in 8 places, all below massif's threshold (1.00%)
 n0: 451 in 30 places, all below massif's threshold (1.00%)
 n1: 416 0x13138A: ??? (in /usr/bin/mpiexec.hydra)
  n1: 416 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 416 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
    n1: 416 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
     n0: 416 0x52B0B95: (below main) (libc-start.c:310)
 n1: 288 0x1372A9: ??? (in /usr/bin/mpiexec.hydra)
  n0: 288 in 6 places, all below massif's threshold (1.00%)
 n1: 264 0x11F0B3: ??? (in /usr/bin/mpiexec.hydra)
  n1: 264 0x11E966: ??? (in /usr/bin/mpiexec.hydra)
   n1: 264 0x11569A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 264 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
     n0: 264 0x52B0B95: (below main) (libc-start.c:310)
 n2: 264 0x12D3EB: ??? (in /usr/bin/mpiexec.hydra)
  n2: 264 0x12D5B3: ??? (in /usr/bin/mpiexec.hydra)
   n1: 240 0x12D8EC: ??? (in /usr/bin/mpiexec.hydra)
    n1: 240 0x12DAFD: ??? (in /usr/bin/mpiexec.hydra)
     n1: 240 0x10E272: ??? (in /usr/bin/mpiexec.hydra)
      n0: 240 0x52B0B95: (below main) (libc-start.c:310)
   n0: 24 in 1 place, below massif's threshold (1.00%)
  n0: 0 in 1 place, below massif's threshold (1.00%)
#-----------
snapshot=28
#-----------
time=474
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=29
#-----------
time=662
mem_heap_B=85638
mem_heap_extra_B=2170
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=30
#-----------
time=664
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=detailed
n4: 85611 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 2387 in 38 places, all below massif's threshold (1.00%)
 n2: 1592 0x532C9B8: strdup (strdup.c:42)
  n1: 942 0x1313BE: ??? (in /usr/bin/mpiexec.hydra)
   n1: 942 0x11C53A: ??? (in /usr/bin/mpiexec.hydra)
    n1: 942 0x1158F3: ??? (in /usr/bin/mpiexec.hydra)
     n1: 942 0x10E4AA: ??? (in /usr/bin/mpiexec.hydra)
      n0: 942 0x52B0B95: (below main) (libc-start.c:310)
  n0: 650 in 87 places, all below massif's threshold (1.00%)
#-----------
snapshot=31
#-----------
time=715
mem_heap_B=85650
mem_heap_extra_B=2174
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=32
#-----------
time=717
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=33
#-----------
time=791
mem_heap_B=85654
mem_heap_extra_B=2170
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=34
#-----------
time=794
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=35
#-----------
time=828
mem_heap_B=85713
mem_heap_extra_B=2175
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=36
#-----------
time=831
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=37
#-----------
time=856
mem_heap_B=85743
mem_heap_extra_B=2177
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=38
#-----------
time=858
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=39
#-----------
time=907
mem_heap_B=85820
mem_heap_extra_B=2180
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=40
#-----------
time=909
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=41
#-----------
time=1239
mem_heap_B=85693
mem_heap_extra_B=2179
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=42
#-----------
time=1243
mem_heap_B=85611
mem_heap_extra_B=2157
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=43
#-----------
time=1268
mem_heap_B=85627
mem_heap_extra_B=2165
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=44
#-----------
time=1270
mem_heap_B=85096
mem_heap_extra_B=2056
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=45
#-----------
time=1272
mem_heap_B=216248
mem_heap_extra_B=2112
mem_stacks_B=0
heap_tree=peak
n4: 216248 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 131120 0x12FB35: ??? (in /usr/bin/mpiexec.hydra)
  n1: 131120 0x13CDE8: ??? (in /usr/bin/mpiexec.hydra)
   n1: 131120 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
    n1: 131120 0x132814: ??? (in /usr/bin/mpiexec.hydra)
     n1: 131120 0x13253B: ??? (in /usr/bin/mpiexec.hydra)
      n1: 131120 0x115EC2: ??? (in /usr/bin/mpiexec.hydra)
       n1: 131120 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
        n0: 131120 0x52B0B95: (below main) (libc-start.c:310)
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n2: 16096 0x12AB4C: ??? (in /usr/bin/mpiexec.hydra)
  n1: 8048 0x110C8A: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x112864: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x12B893: ??? (in /usr/bin/mpiexec.hydra)
     n1: 8048 0x111038: ??? (in /usr/bin/mpiexec.hydra)
      n1: 8048 0x113F60: ??? (in /usr/bin/mpiexec.hydra)
       n1: 8048 0x10DB19: ??? (in /usr/bin/mpiexec.hydra)
        n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
  n1: 8048 0x12ADF7: ??? (in /usr/bin/mpiexec.hydra)
   n1: 8048 0x12B367: ??? (in /usr/bin/mpiexec.hydra)
    n1: 8048 0x10E342: ??? (in /usr/bin/mpiexec.hydra)
     n0: 8048 0x52B0B95: (below main) (libc-start.c:310)
 n0: 3496 in 40 places, all below massif's threshold (1.00%)
#-----------
snapshot=46
#-----------
time=1275
mem_heap_B=215694
mem_heap_extra_B=1666
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=47
#-----------
time=1277
mem_heap_B=66184
mem_heap_extra_B=144
mem_stacks_B=0
heap_tree=detailed
n2: 66184 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 648 in 42 places, all below massif's threshold (1.00%)
#-----------
snapshot=48
#-----------
time=1283
mem_heap_B=66176
mem_heap_extra_B=128
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=49
#-----------
time=1283
mem_heap_B=65984
mem_heap_extra_B=64
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=50
#-----------
time=1283
mem_heap_B=65968
mem_heap_extra_B=56
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=51
#-----------
time=1283
mem_heap_B=65840
mem_heap_extra_B=48
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=52
#-----------
time=1283
mem_heap_B=65824
mem_heap_extra_B=40
mem_stacks_B=0
heap_tree=detailed
n2: 65824 (heap allocation functions) malloc/new/new[], --alloc-fns, etc.
 n1: 65536 0x117326: ??? (in /usr/bin/mpiexec.hydra)
  n1: 65536 0x13792E: ??? (in /usr/bin/mpiexec.hydra)
   n1: 65536 0x115D79: ??? (in /usr/bin/mpiexec.hydra)
    n1: 65536 0x10EAC0: ??? (in /usr/bin/mpiexec.hydra)
     n0: 65536 0x52B0B95: (below main) (libc-start.c:310)
 n0: 288 in 42 places, all below massif's threshold (1.00%)
#-----------
snapshot=53
#-----------
time=1283
mem_heap_B=65696
mem_heap_extra_B=32
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=54
#-----------
time=1283
mem_heap_B=65680
mem_heap_extra_B=24
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=55
#-----------
time=1284
mem_heap_B=65552
mem_heap_extra_B=16
mem_stacks_B=0
heap_tree=empty
#-----------
snapshot=56
#-----------
time=1284
mem_heap_B=65536
mem_heap_extra_B=8
mem_stacks_B=0
heap_tree=empty
