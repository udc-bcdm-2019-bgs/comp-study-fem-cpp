RANGE                                  SIZE  STATE REMOVABLE   BLOCK
0x0000000000000000-0x000000000fffffff  256M online        no       0
0x0000000010000000-0x000000002fffffff  512M online       yes     1-2
0x0000000030000000-0x000000004fffffff  512M online        no     3-4
0x0000000050000000-0x000000007fffffff  768M online       yes     5-7
0x0000000080000000-0x000000008fffffff  256M online        no       8
0x0000000100000000-0x000000017fffffff    2G online        no   16-23
0x0000000180000000-0x000000018fffffff  256M online       yes      24
0x0000000190000000-0x000000061fffffff 18.3G online        no   25-97
0x0000000620000000-0x000000062fffffff  256M online       yes      98
0x0000000630000000-0x0000000a8fffffff 17.5G online        no  99-168
0x0000000a90000000-0x0000000aafffffff  512M online       yes 169-170
0x0000000ab0000000-0x0000000affffffff  1.3G online        no 171-175
0x0000000b00000000-0x0000000b0fffffff  256M online       yes     176
0x0000000b10000000-0x0000000bcfffffff    3G online        no 177-188
0x0000000bd0000000-0x0000000bdfffffff  256M online       yes     189
0x0000000be0000000-0x0000000c0fffffff  768M online        no 190-192
0x0000000c10000000-0x0000000c1fffffff  256M online       yes     193
0x0000000c20000000-0x0000000c9fffffff    2G online        no 194-201
0x0000000ca0000000-0x0000000cafffffff  256M online       yes     202
0x0000000cb0000000-0x0000000cbfffffff  256M online        no     203
0x0000000cc0000000-0x0000000ccfffffff  256M online       yes     204
0x0000000cd0000000-0x0000000d2fffffff  1.5G online        no 205-210
0x0000000d30000000-0x0000000d3fffffff  256M online       yes     211
0x0000000d40000000-0x000000106fffffff 12.8G online        no 212-262

Memory block size:       256M
Total online memory:      64G
Total offline memory:      0B
